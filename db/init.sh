#!/bin/sh

# (Re)initializes a PostgreSQL database, i.e. drops it if it exists, creates a new one, creates a role and sets it as owner of the database.
# NOTE: PostgreSQL admin password can be overridden via a parameter.

set -e

# Exit the entire script when something fails
set -e
function exit_trap() {
    echo "The last command finished with exit code $?. Exiting."
}
trap exit_trap ERR

###

if [ -z $3 ]; then
    echo "Usage: $0 [database] [username] [password] [admin password override]"
    exit 1
fi

hostname="localhost"
database="$1"
username="$2"
password="$3"
adm_username="postgres"
adm_password_override="$4"

echo "init.sh: connecting to ${hostname} as role ${adm_username} to (re)initialize the database ${database}"

if [ -z $adm_password_override ]; then
    jdbc="postgresql://${adm_username}@${hostname}"
else
    jdbc="postgresql://${adm_username}:${adm_password_override}@${hostname}"
fi

psql $jdbc << EOF
\x
DROP DATABASE IF EXISTS "${database}";
CREATE DATABASE "${database}";

DO \$\$
BEGIN
  IF NOT EXISTS (SELECT * FROM pg_roles WHERE rolname = '${username}') THEN
    CREATE ROLE "${username}" LOGIN PASSWORD '${password}';
  END IF;

  IF EXISTS (SELECT * FROM pg_roles WHERE rolname = '${username}') THEN
    REVOKE ALL ON DATABASE ${database} FROM "${username}";
    REVOKE ALL ON ALL TABLES IN SCHEMA public FROM "${username}";
    REVOKE ALL ON ALL SEQUENCES IN SCHEMA public FROM "${username}";
  END IF;
END\$\$;

ALTER DATABASE "${database}" OWNER TO "${username}";
EOF

psql $jdbc/$database << EOF 
ALTER SCHEMA public OWNER TO ${username};
EOF
