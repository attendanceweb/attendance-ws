# Attendance web service #

## Environment variables ##
http.port (default: 8080)
`http.context_path` (default: '/')
db.url (default: `postgresql://localhost/attendance`)
db.username
db.password
env (default: 'production')
notification.host (default: 'localhost')
notification.port (default: 587)
notification.user
notification.password
notification.from
notification.to
security.allowedOrigins (default: `http://localhost:9000`)

## Testing ##
Preparing a local test database:
Install PostgreSQL locally and and open for md5 authentication. Set up a test database:
```
$ psql
postgres=# create database attendance_test;
postgres=# create user attendance_test with password 'test';
postgres=# grant all privileges on database attendance_test to attendance_test;
```

Running tests:
```
./sbt test
```

## Run in development ##
Copy the example development configuration:
```
cp conf/dev-config.conf.example conf/dev-config.conf
```

Adjust the configuration.

Start the server in watch mode (automatic restart after a change):
```
sbt '~; compile; jetty:start'
```

## Run in production ##
Set environment variables.

Build and run: ... TODO

## Importing data ##
Identity sequences have to be adjusted after a manual import. Execute the following for all tables, replacing 'xx' with table name:
```
select setval('xx_id_seq', max(id)) FROM xx
```

## Heroku
You need the plugin heroku-config to populate configuration variables:
```
heroku plugins:install heroku-config
```

Push configuration variables to Heroku:
```
heroku config:push -a <app name>
```

Stop a heroku app:
```
heroku ps:scale web=0
```

Deploy:
```
heroku login
heroku container:login
heroku container:push web -a <app name>
heroku container:release web -a <app name>
```

