name := "attendance-web"
version := "1.0"

scalaVersion := "2.12.8"
val ScalatraVersion = "2.6.+"

resolvers += Classpaths.typesafeReleases
resolvers += "lightshed-maven" at "http://dl.bintray.com/content/lightshed/maven"

val jettyDependencies = Seq(
  "org.json4s" %% "json4s-jackson" % "3.6.4",
  "org.json4s" %% "json4s-ext" % "3.6.4",
  "javax.servlet" % "javax.servlet-api" % "4.0.1" % "provided",
  "org.eclipse.jetty" % "jetty-webapp" % "9.4.14.v20181114" % "compile;container"
)

libraryDependencies ++= jettyDependencies

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.scalatra" %% "scalatra-specs2" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime"
)
libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "42.2.5",
  "org.playframework.anorm" %% "anorm" % "2.6.2",
  "com.jolbox" % "bonecp" % "0.8.0.RELEASE",
  "org.flywaydb" % "flyway-core" % "5.2.4"
)
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
libraryDependencies += "ch.lightshed" %% "courier" % "0.1.4"
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5"

lazy val initTestDb = taskKey[Unit]("Task that initializes the test DB")
initTestDb in Test := {
  import sys.process._
  val exitCode = "./db/init.sh attendance_test attendance_test test postgres".!
  if (exitCode != 0) throw new Exception
}

test in Test := {
  val init = (initTestDb in Test).value
  (test in Test).value
}

parallelExecution in Test := false
test in assembly := {}

mainClass in assembly := Some("attendance.JettyLauncher")
assemblyJarName in assembly := "attendance-web.jar"

enablePlugins(ScalatraPlugin)
