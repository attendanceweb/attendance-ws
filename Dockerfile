FROM openjdk:11

MAINTAINER Piotr Kamisinski <piotr.kamisinski@ktd.krakow.pl>

RUN mkdir -p /app

ADD target/scala-2.12/attendance-web.jar /app/

# Only used for testing (set the PORT environment variable to the value below).
# Heroku doesn't support EXPOSE
EXPOSE 8080

RUN useradd attendance
USER attendance

CMD java -Dhttp.port=$PORT -Ddb.url=$DATABASE_URL -Dsecurity.allowedOrigins=$ALLOWED_ORIGINS -Dnotification.from=$NOTIFICATION_FROM -Dnotification.toChairman=$NOTIFICATION_TOCHAIRMAN -Dnotification.host=$NOTIFICATION_HOST -Dnotification.port=$NOTIFICATION_PORT -Dnotification.user=$NOTIFICATION_USER -Dnotification.password=$NOTIFICATION_PASSWORD -jar /app/attendance-web.jar


