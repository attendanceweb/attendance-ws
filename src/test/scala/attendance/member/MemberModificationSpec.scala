package attendance.member

import attendance.db.{ConnectionPool, TestDbProvider}
import attendance.util.datetime.DateUtil._
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

class MemberModificationSpec extends FlatSpec with BeforeAndAfterAll {

  implicit val db: ConnectionPool = TestDbProvider.pool(getClass.getName)

  val memberProvider = new MemberProvider()
  val memberModification = new MemberModification(memberProvider)

  override def beforeAll(): Unit = {
    TestDbProvider.setup(db, getClass.getName)
  }

  "member objects with identical names, section_id and e-mail addresses" should "be regarded as identical" in {
    val member = Member(0, "A", "B", 1, localDateTime, None, None)
    val modifyRequest = MemberModifyRequest("A", "B", 1)

    val different = memberModification.isDifferent(member, modifyRequest)

    assert(!different)
  }

  "member objects with different first_names" should "be regarded as different" in {
    val member = Member(0, "A", "B", 1, localDateTime, None, None)
    val modifyRequest = MemberModifyRequest("C", "B", 1)

    val different = memberModification.isDifferent(member, modifyRequest)

    assert(different)
  }

  "member objects with different last_names" should "be regarded as different" in {
    val member = Member(0, "A", "B", 1, localDateTime, None, None)
    val modifyRequest = MemberModifyRequest("A", "C", 1)

    val different = memberModification.isDifferent(member, modifyRequest)

    assert(different)
  }

  "member objects with different sections" should "be regarded as different" in {
    val member = Member(0, "A", "B", 1, localDateTime, None, None)
    val modifyRequest = MemberModifyRequest("A", "B", 2)

    val different = memberModification.isDifferent(member, modifyRequest)

    assert(different)
  }

  "member objects with different e-mail addresses" should "be regarded as different" in {
    val member = Member(0, "A", "B", 1, localDateTime, None, None)
    val modifyRequest = MemberModifyRequest("A", "B", 2)

    val different = memberModification.isDifferent(member, modifyRequest)

    assert(different)
  }

}
