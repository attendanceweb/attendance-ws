package attendance.member

import attendance.attendance.AttendanceProvider
import attendance.db.{ConnectionPool, TestDbProvider}
import attendance.rehearsal.{Rehearsal, RehearsalProvider}
import attendance.section.{SectionContactProvider, SectionProvider}
import attendance.util.datetime.DateUtil._
import org.scalatest._

class MemberProviderSpec extends FlatSpec with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit val db: ConnectionPool = TestDbProvider.pool(getClass.getName)

  val sectionProvider = new SectionProvider()
  val memberProvider = new MemberProvider()
  val sectionContactProvider = new SectionContactProvider()
  val rehearsalProvider = new RehearsalProvider()
  val attendanceProvider = new AttendanceProvider()

  override def beforeAll(): Unit = {
    TestDbProvider.setup(db, getClass.getName)
  }

  override def beforeEach(): Unit = {
    attendanceProvider.removeAll()
    memberProvider.removeAll()
    sectionContactProvider.removeAll()
    sectionProvider.removeAll()
    rehearsalProvider.removeAll()
  }

  "find active for section contact between dates" should "return none if no members" in {
    val ret = memberProvider.findActiveBySectionContact(0, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))
    assert(ret.isEmpty)
  }

  it should "return none if member active before start date" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-01T18:30:00"), Some(localDateTime("2017-01-05T21:00:00")))

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.isEmpty)
  }

  it should "return one if member active with start before period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-03T18:30:00"), None)

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  it should "return one if member active exactly within period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-10T18:30:00"), Some(localDateTime("2017-01-10T21:00:00")))

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  it should "return one if member active within period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-11T18:30:00"), Some(localDateTime("2017-01-15T21:00:00")))

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-17T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  it should "return one if member active with start within period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-10T18:30:00"), None)

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  it should "return one if member active with start after period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-17T18:30:00"), None)

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.isEmpty)
  }

  it should "return one if member active start and end after period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, memberId, "a@a.com")
    val memberActiveId = memberProvider.addActivity(memberId, localDateTime("2017-01-17T18:30:00"), Some(localDateTime("2017-01-24T18:30:00")))

    val ret = memberProvider.findActiveBySectionContact(memberId, localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-10T21:00:00"))

    assert(ret.isEmpty)
  }

  "find active in time period" should "ignore inactive in that period" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")
    val memberActive1Id = memberProvider.addActivity(member1Id, localDateTime("2017-01-17T18:30:00"), Some(localDateTime("2017-01-24T18:30:00")))
    val memberActive2Id = memberProvider.addActivity(member2Id, localDateTime("2017-01-27T18:30:00"), Some(localDateTime("2017-02-04T18:30:00")))

    val ret = memberProvider.findActive(localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-25T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  "find active at a time instant" should "ignore inactive" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")
    val memberActive1Id = memberProvider.addActivity(member1Id, localDateTime("2017-01-17T18:30:00"), Some(localDateTime("2017-01-24T18:30:00")))
    val memberActive2Id = memberProvider.addActivity(member2Id, localDateTime("2017-01-27T18:30:00"), Some(localDateTime("2017-02-04T18:30:00")))

    val ret = memberProvider.findActive(localDateTime("2017-01-10T18:30:00"), localDateTime("2017-01-25T21:00:00"))

    assert(ret.lengthCompare(1) == 0)
  }

  it should "include members activated or deactivated at that time instant" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    val sectionContactId = sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")
    val memberActive1Id = memberProvider.addActivity(member1Id, localDateTime("2017-01-17T18:30:00"), Some(localDateTime("2017-01-24T18:30:00")))
    val memberActive2Id = memberProvider.addActivity(member2Id, localDateTime("2017-01-24T18:30:00"), Some(localDateTime("2017-02-04T18:30:00")))

    val ret = memberProvider.findActive(localDateTime("2017-01-24T18:30:00"))

    assert(ret.length == 2)
  }

  "find members that have been inactive for over a year test 1" should "return the correct members" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")

    val now = localDateTime
    memberProvider.addActivity(member1Id, now.minusYears(2), Some(now.minusYears(2).plusMonths(2)))
    memberProvider.addActivity(member1Id, now.minusYears(2).plusMonths(4), Some(now.minusYears(1)))
    memberProvider.addActivity(member2Id, now.minusYears(2), Some(now.minusYears(1)))

    val ret = memberProvider.findInactiveForOverOneYear()

    assert(ret.length == 2)
    assert(ret(0).member_id == member1Id)
    assert(ret(1).member_id == member2Id)
  }

  "find members that have been inactive for over a year test 2" should "return the correct members" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")

    val now = localDateTime
    memberProvider.addActivity(member1Id, now.minusYears(2), Some(now.minusYears(1)))
    memberProvider.addActivity(member2Id, localDateTime, None)

    val ret = memberProvider.findInactiveForOverOneYear()

    assert(ret.length == 1)
    assert(ret.head.member_id == member1Id)
  }

  "find members that have been inactive for over a year test 3" should "return the correct members" in {
    val sectionId = sectionProvider.add("Test Section")
    val member1Id = 1
    val member2Id = 2
    memberProvider.add(member1Id, "Test", "Member", sectionId.get)
    memberProvider.add(member2Id, "Test", "Member2", sectionId.get)
    sectionContactProvider.add(sectionId.get, member1Id, "a@a.com")

    val now = localDateTime
    memberProvider.addActivity(member1Id, now.minusYears(2), Some(now.minusYears(2).plusMonths(2)))
    memberProvider.addActivity(member1Id, now.minusYears(2).plusMonths(4), Some(now.minusYears(1)))
    memberProvider.addActivity(member1Id, now.minusYears(1), Some(now.minusYears(1).plusMonths(1)))
    memberProvider.addActivity(member2Id, now.minusYears(2), Some(now.minusYears(1)))

    val ret = memberProvider.findInactiveForOverOneYear()

    assert(ret.length == 1)
    assert(ret.head.member_id == member2Id)
  }

  "change member activity period" should "be a success if no registered attendance falls outside the new period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(1, localDateTime("2017-01-17T18:30"), localDateTime("2017-01-17T21:00"), obligatory = true, None))
    val rehearsalId2 = rehearsalProvider.add(Rehearsal(2, localDateTime("2017-01-24T18:30"), localDateTime("2017-01-24T21:00"), obligatory = true, None))

    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    attendanceProvider.add(memberId, rehearsalId1.get, 1)
    attendanceProvider.add(memberId, rehearsalId2.get, 1)

    val status = memberProvider.modify(memberId, localDateTime("2017-01-16T18:30"))

    assert(status == MemberActivateStatus.ChangedActivityPeriod)
  }

  it should "return error if registered attendance falls outside the new activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(1, localDateTime("2017-01-17T18:30"), localDateTime("2017-01-17T21:00"), obligatory = true, None))
    val rehearsalId2 = rehearsalProvider.add(Rehearsal(2, localDateTime("2017-01-24T18:30"), localDateTime("2017-01-24T21:00"), obligatory = true, None))

    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    attendanceProvider.add(memberId, rehearsalId1.get, 1)
    attendanceProvider.add(memberId, rehearsalId2.get, 1)

    val status = memberProvider.modify(memberId, localDateTime("2017-01-24T18:30"))

    assert(status == MemberActivateStatus.ChangeWouldOrphanAttendance)
  }


  "activate a member" should "succeed if the member is not already active" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)

    val status = memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    assert(status == MemberActivateStatus.Activated)
  }

  it should "fail if the member is active" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    val status = memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    assert(status == MemberActivateStatus.AlreadyActive)
  }

  "deactivate a member" should "succeed if the member is active" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    val status = memberProvider.deactivate(memberId, localDateTime("2017-04-17T18:30"))

    assert(status == MemberActivateStatus.Deactivated)
  }

  it should "fail if the member is not active" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)

    val status = memberProvider.deactivate(memberId, localDateTime("2017-04-17T18:30"))

    assert(status == MemberActivateStatus.AlreadyInactive)
  }

  it should "succeed if date_to is the same as date_from" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    val status = memberProvider.deactivate(memberId, localDateTime("2017-01-17T18:30"))

    assert(status == MemberActivateStatus.Deactivated)
  }

  "activeAt" should "return nothing if before the first activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-01-16T18:30"))

    assert(activity.isEmpty)
  }

  it should "return nothing if after the last activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-02-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-03-16T18:30"))

    assert(activity.isEmpty)
  }

  it should "return activity period if at the start of an activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(memberId, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-02-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-01-17T18:30"))

    assert(activity.nonEmpty)
  }

  it should "return nothing if at the end of an activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-02-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-02-17T18:30"))

    assert(activity.isEmpty)
  }

  it should "return activity period if inside a closed activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-02-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-01-27T18:30"))

    assert(activity.nonEmpty)
  }

  it should "return activity period if inside an open activity period" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-01-27T18:30"))

    assert(activity.nonEmpty)
  }

  it should "return nothing between two closed activity periods" in {
    val sectionId = sectionProvider.add("Test Section")
    val memberId = 1
    memberProvider.add(1, "Test", "Member", sectionId.get)
    memberProvider.activate(memberId, localDateTime("2017-01-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-02-17T18:30"))
    memberProvider.activate(memberId, localDateTime("2017-03-17T18:30"))
    memberProvider.deactivate(memberId, localDateTime("2017-04-17T18:30"))

    val activity = memberProvider.activityAt(memberId, localDateTime("2017-02-27T18:30"))

    assert(activity.isEmpty)
  }
}