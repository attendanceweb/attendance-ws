package attendance.rehearsal

import attendance.db._
import attendance.util.datetime.DateUtil._
import org.scalatest._

class RehearsalSpec extends FlatSpec with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit val db: ConnectionPool = TestDbProvider.pool(getClass.getName)
  val rehearsalProvider = new RehearsalProvider()

  override def beforeAll(): Unit = {
    TestDbProvider.setup(db, getClass.getName)
  }

  override def beforeEach(): Unit = {
    rehearsalProvider.removeAll()
  }

  "closest" should "return none if no rehearsals" in {
    val ret = rehearsalProvider.closestTo(localDateTime)
    assert(ret.isEmpty)
  }

  it should "return if one rehearsal before" in {
    val rehearsal = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsal)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-11T18:30"))

    assert(ret.isDefined)
  }

  it should "return if one rehearsal after" in {
    val rehearsal = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsal)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-09T18:30"))

    assert(ret.isDefined)
  }

  it should "return earlier rehearsal if it is closer 1" in {
    val dateTimeStartEarlier = localDateTime("2017-01-10T18:30")
    val rehearsalEarlier = Rehearsal(1, dateTimeStartEarlier, localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val dateTimeStartLater = localDateTime("2017-01-17T18:30")
    val rehearsalLater = Rehearsal(2, dateTimeStartLater, localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-09T18:30"))

    // we have to compare string representations due to a difference in representation of timezone in DateTime objects,
    // see http://stackoverflow.com/questions/21002385/datetime-does-not-equal-itself-after-unserialization
    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(dateTimeStartEarlier))
  }
  it should "return earlier rehearsal if it is closer 2" in {
    val dateTimeStartEarlier = localDateTime("2017-01-10T18:30")
    val rehearsalEarlier = Rehearsal(1, dateTimeStartEarlier, localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val dateTimeStartLater = localDateTime("2017-01-17T18:30")
    val rehearsalLater = Rehearsal(2, dateTimeStartLater, localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-13T18:30"))

    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(dateTimeStartEarlier))
  }

  it should "return later rehearsal if it is closer 1" in {
    val dateTimeStartEarlier = localDateTime("2017-01-10T18:30")
    val rehearsalEarlier = Rehearsal(1, dateTimeStartEarlier, localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val dateTimeStartLater = localDateTime("2017-01-17T18:30")
    val rehearsalLater = Rehearsal(2, dateTimeStartLater, localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-14T18:30"))

    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(dateTimeStartLater))
  }

  it should "return later rehearsal if it is closer 2" in {
    val dateTimeStartEarlier = localDateTime("2017-01-10T18:30")
    val rehearsalEarlier = Rehearsal(1, dateTimeStartEarlier, localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val dateTimeStartLater = localDateTime("2017-01-17T18:30")
    val rehearsalLater = Rehearsal(2, dateTimeStartLater, localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.closestTo(localDateTime("2017-01-18T18:30"))

    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(dateTimeStartLater))
  }

  "previous" should "return none if no earlier rehearsals" in {
    val rehearsal = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsal)

    val ret = rehearsalProvider.previous(rehearsal)

    assert(ret.isEmpty)
  }

  it should "return rehearsal if exists one earlier" in {
    val rehearsalEarlier = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val rehearsalLater = Rehearsal(2, localDateTime("2017-01-17T18:30"), localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.previous(rehearsalLater)

    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(rehearsalEarlier.date_start))
  }

  "next" should "return none if no later rehearsals" in {
    val rehearsal = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsal)

    val ret = rehearsalProvider.next(rehearsal)

    assert(ret.isEmpty)
  }

  it should "return rehearsal if exists one later" in {
    val rehearsalEarlier = Rehearsal(1, localDateTime("2017-01-10T18:30"), localDateTime("2017-01-10T21:00"), obligatory = true, None)
    val rehearsalLater = Rehearsal(2, localDateTime("2017-01-17T18:30"), localDateTime("2017-01-17T21:00"), obligatory = true, None)
    rehearsalProvider.add(rehearsalEarlier)
    rehearsalProvider.add(rehearsalLater)

    val ret = rehearsalProvider.next(rehearsalEarlier)

    assert(localDateTimeToString(ret.get.date_start) == localDateTimeToString(rehearsalLater.date_start))
  }

}