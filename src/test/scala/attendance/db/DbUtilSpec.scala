package attendance.db

import org.scalatest.FlatSpec

class DbUtilSpec extends FlatSpec {

  "database URL string with host, port, username and password" should "parse correctly to JDBC string" in {
    val dbUrl = "postgres://xuwrzawmuwimcc:5f0f111bd01dd72862ddf8b650ffc27b512055c0e8d163ff96f70bfc648ebf63@ec2-54-217-205-90.eu-west-1.compute.amazonaws.com:5432/dpekq9d9pdjbv"
    val expected = "jdbc:postgresql://ec2-54-217-205-90.eu-west-1.compute.amazonaws.com:5432/dpekq9d9pdjbv?user=xuwrzawmuwimcc&password=5f0f111bd01dd72862ddf8b650ffc27b512055c0e8d163ff96f70bfc648ebf63"
    assert(DbUtil.herokuDbUrlToJdbcUrl(dbUrl) == expected)
  }

  "JDBC string" should "be returned unchanged" in {
    val dbUrl = "jdbc:postgresql://ec2-54-217-205-90.eu-west-1.compute.amazonaws.com:5432/dpekq9d9pdjbv?user=xuwrzawmuwimcc&password=5f0f111bd01dd72862ddf8b650ffc27b512055c0e8d163ff96f70bfc648ebf63"
    assert(DbUtil.herokuDbUrlToJdbcUrl(dbUrl) == dbUrl)
  }
}
