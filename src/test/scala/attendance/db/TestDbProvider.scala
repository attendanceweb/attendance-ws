package attendance.db

import anorm._
import attendance.util.TryWith

import scala.util.Try

object TestDbProvider extends DatabaseInit {

  private val driver = "org.postgresql.Driver"
  private def jdbcUri(name: String) = s"jdbc:postgresql://localhost/attendance_test"
  private val username = "attendance_test"
  private val password = "test"

  def pool(name: String): ConnectionPool = initDatabase(name, jdbcUri(name), "attendance_test", "test").get

  def setup(db: ConnectionPool, name: String): Unit = {
    resetDb(db)
    migrate(name)
  }

  private def resetDb(db: ConnectionPool): Unit = {
    println("Dropping and recreating the public schema")
    // TODO for some reason this doesn't throw an exception if SQL fails
    TryWith(db.connection.get) { implicit con =>
      SQL"""
           DROP SCHEMA public CASCADE;
           CREATE SCHEMA public;
           GRANT ALL ON SCHEMA public TO postgres;
           GRANT ALL ON SCHEMA public TO public;
        """.execute()
    }
  }

  private def migrate(name: String): Try[Unit] = {
    migrate(jdbcUri(name), username, password)
  }
}
