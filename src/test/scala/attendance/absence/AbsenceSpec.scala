package attendance.absence

import attendance.project.Project
import org.scalatest._

class AbsenceSpec extends FlatSpec {

  "sameMemberAndProjectsAs" should "work if the first MemberAbsence has more projects than the second" in {
    val first = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty), MemberAbsenceForProject(Some(Project(2, "P", List.empty)), List.empty)))
    val second = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty)))

    val result = first.sameMemberAndProjectsAs(second)

    assert(result == false)
  }

  it should "work if the second MemberAbsence has more projects than the first" in {
    val first = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty)))
    val second = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty), MemberAbsenceForProject(Some(Project(2, "P", List.empty)), List.empty)))

    val result = first.sameMemberAndProjectsAs(second)

    assert(result == false)
  }

  it should "work if both MemberAbsence have the same projects" in {
    val first = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty)))
    val second = MemberAbsence(1, "A", "B", 1, 1, true, List(MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty)))

    val result = first.sameMemberAndProjectsAs(second)

    assert(result == true)
  }

  it should "work if MemberAbsence differ with member_id" in {
    val memberAbsenceForProject1 = MemberAbsenceForProject(Some(Project(1, "P", List.empty)), List.empty)
    val memberAbsenceForProject2 = MemberAbsenceForProject(Some(Project(2, "P", List.empty)), List.empty)
    val first = MemberAbsence(1, "A", "B", 1, 1, true, List(memberAbsenceForProject1, memberAbsenceForProject2))
    val second = MemberAbsence(2, "A", "B", 1, 1, true, List(memberAbsenceForProject1, memberAbsenceForProject2))

    val result = first.sameMemberAndProjectsAs(second)

    assert(result == false)
  }

}