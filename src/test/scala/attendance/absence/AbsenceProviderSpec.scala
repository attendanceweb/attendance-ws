package attendance.absence

import attendance.attendance.AttendanceProvider
import attendance.db.{ConnectionPool, TestDbProvider}
import attendance.member.MemberProvider
import attendance.rehearsal.{Rehearsal, RehearsalProvider}
import attendance.section.{SectionContactProvider, SectionProvider}
import attendance.util.datetime.DateUtil._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec}

class AbsenceProviderSpec extends FlatSpec with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit val db: ConnectionPool = TestDbProvider.pool(getClass.getName)

  val sectionProvider = new SectionProvider()
  val memberProvider = new MemberProvider()
  val sectionContactProvider = new SectionContactProvider()
  val rehearsalProvider = new RehearsalProvider()
  val memberAttendanceProvider = new AttendanceProvider()
  val memberAbsenceProvider = new AbsenceProvider()

  override def beforeAll(): Unit = {
    TestDbProvider.setup(db, getClass.getName)
  }

  override def beforeEach(): Unit = {
    memberAttendanceProvider.removeAll()
    sectionContactProvider.removeAll()
    memberProvider.removeAll()
    sectionProvider.removeAll()
    rehearsalProvider.removeAll()
  }

  "for one member that has reached absence limit and another that has not" should "return one member absence" in {
    val sectionId1 = sectionProvider.add("A")
    val sectionId2 = sectionProvider.add("B")
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(-1, localDateTime("2018-05-29T18:30"), localDateTime("2018-05-29T21:00"), obligatory = true, None))
    val rehearsalId2 = rehearsalProvider.add(Rehearsal(-1, localDateTime("2018-05-22T18:30"), localDateTime("2018-05-22T21:00"), obligatory = true, None))
    val memberId1_1Try = memberProvider.add(1, "A", "B", sectionId1.get)
    val memberId2_1Try = memberProvider.add(2, "C", "D", sectionId2.get)
    val absence: List[MemberAbsenceForSectionContact] = memberId1_1Try.map { _ =>
      memberProvider.activate(1, localDateTime("2018-03-29T00:00"))
      memberId2_1Try.map { _ =>
        memberProvider.activate(2, localDateTime("2018-03-28T00:00"))

        val sc1 = sectionContactProvider.add(sectionId1.get, 1, "")
        val sc2 = sectionContactProvider.add(sectionId2.get, 2, "")

        memberAttendanceProvider.add(1, rehearsalId1.get, 1)
        memberAttendanceProvider.add(2, rehearsalId1.get, 0)
        memberAttendanceProvider.add(1, rehearsalId2.get, 0)
        memberAttendanceProvider.add(2, rehearsalId2.get, 0)

        val startDate = localDateTime("2018-01-01T00:00")
        val endDate = localDateTime("2018-05-29T22:00")
        memberAbsenceProvider.absenceForSectionContactsBetween(2, startDate, endDate)
      }.get
    }.get
    assert(absence.size == 2)
    assert(absence(0).membersAbsence.size == 0)
    assert(absence(1).membersAbsence.size == 1)
    assert(absence(1).membersAbsence(0).absence.head.absence.size == 2)
  }

  "for a member that switched section and has reached absence limit" should "return correct absence count" in {
    val sectionId1 = sectionProvider.add("A")
    val sectionId2 = sectionProvider.add("B")
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(-1, localDateTime("2018-05-29T18:30"), localDateTime("2018-05-29T21:00"), obligatory = true, None))
    val rehearsalId2 = rehearsalProvider.add(Rehearsal(-1, localDateTime("2018-05-22T18:30"), localDateTime("2018-05-22T21:00"), obligatory = true, None))
    val memberId1_1Try = memberProvider.add(1, "A", "B", sectionId1.get)

    val absence: List[MemberAbsenceForSectionContact] = memberId1_1Try.map { _ =>
      memberProvider.activate(1, localDateTime("2018-03-22T00:00"))
      memberProvider.modify(1, "A", "B", sectionId2.get, localDateTime("2018-05-29T00:00"))

      sectionContactProvider.add(sectionId1.get, 1, "")
      sectionContactProvider.add(sectionId2.get, 1, "")

      memberAttendanceProvider.add(1, rehearsalId1.get, 1)
      memberAttendanceProvider.add(1, rehearsalId2.get, 0)

      val startDate = localDateTime("2018-01-01T00:00")
      val endDate = localDateTime("2018-05-29T22:00")
      memberAbsenceProvider.absenceForSectionContactsBetween(1, startDate, endDate)
    }.get
    assert(absence.size == 1)
    assert(absence(0).membersAbsence.size == 1)
    assert(absence(0).membersAbsence(0).absence.head.absence.size == 1)
  }

}