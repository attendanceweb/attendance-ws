package attendance.report

import java.time.LocalDateTime

import attendance.attendance.AttendanceProvider
import attendance.db.{ConnectionPool, TestDbProvider}
import attendance.member.MemberProvider
import attendance.rehearsal.{Rehearsal, RehearsalProvider}
import attendance.section.{SectionContactProvider, SectionProvider}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec}

class MissingAttendanceProviderSpec extends FlatSpec with BeforeAndAfterAll with BeforeAndAfterEach {


  implicit val db: ConnectionPool = TestDbProvider.pool(getClass.getName)

  val sectionProvider = new SectionProvider()
  val memberProvider = new MemberProvider()
  val sectionContactProvider = new SectionContactProvider()
  val rehearsalProvider = new RehearsalProvider()
  val memberAttendanceProvider = new AttendanceProvider()

  val missingAttendanceProvider = new MissingAttendanceProvider()

  override def beforeAll(): Unit = {
    TestDbProvider.setup(db, getClass.getName)
  }

  override def beforeEach(): Unit = {
    memberAttendanceProvider.removeAll()
    sectionContactProvider.removeAll()
    memberProvider.removeAll()
    sectionProvider.removeAll()
    rehearsalProvider.removeAll()
  }

  "for two active members with missing attendance" should "return missing attendance for both members" in {
    val sectionId1 = sectionProvider.add("A")
    val sectionId2 = sectionProvider.add("B")
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(-1, LocalDateTime.parse("2018-05-29T18:30"), LocalDateTime.parse("2018-05-29T21:00"), obligatory = true, None))
    val memberId1_1Try = memberProvider.add(1, "A", "B", sectionId1.get)
    val memberId2_1Try = memberProvider.add(2, "C", "D", sectionId2.get)
    val missing: List[MissingAttendanceForSectionContact] = memberId1_1Try.map { _ =>
      memberProvider.activate(1, LocalDateTime.parse("2018-03-29T00:00"))
      memberId2_1Try.map { _ =>
        memberProvider.activate(2, LocalDateTime.parse("2018-03-28T00:00"))
        sectionContactProvider.add(sectionId1.get, 1, "")
        sectionContactProvider.add(sectionId2.get, 2, "")

        val startDate = LocalDateTime.parse("2018-01-01T00:00")
        val endDate = LocalDateTime.parse("2018-05-29T22:00")
        missingAttendanceProvider.find(startDate, endDate).get
      }.get
    }.get
    assert(missing.size == 2)
    assert(missing(0).forRehearsals.size == 1)
    assert(missing(0).forRehearsals(0).members.size == 1)
    assert(missing(1).forRehearsals.size == 1)
    assert(missing(1).forRehearsals(0).members.size == 1)
  }

  "for one active member and one inactive with missing attendance" should "return missing attendance for one member" in {
    val sectionId1 = sectionProvider.add("A")
    val sectionId2 = sectionProvider.add("B")
    val rehearsalId1 = rehearsalProvider.add(Rehearsal(-1, LocalDateTime.parse("2018-04-29T18:30"), LocalDateTime.parse("2018-04-29T21:00"), obligatory = true, None))
    val memberId1_1Try = memberProvider.add(1, "A", "B", sectionId1.get)
    val memberId2_1Try = memberProvider.add(2, "C", "D", sectionId2.get)
    val missing: List[MissingAttendanceForSectionContact] = memberId1_1Try.map { _ =>
      memberProvider.activate(1, LocalDateTime.parse("2018-03-29T00:00"))
      memberId2_1Try.map { _ =>
        memberProvider.activate(2, LocalDateTime.parse("2018-05-28T00:00"))
        sectionContactProvider.add(sectionId1.get, 1, "")
        sectionContactProvider.add(sectionId2.get, 2, "")

        val startDate = LocalDateTime.parse("2018-01-01T00:00")
        val endDate = LocalDateTime.parse("2018-04-29T22:00")
        missingAttendanceProvider.find(startDate, endDate).get
      }.get
    }.get
    assert(missing.size == 2)
    assert(missing(0).forRehearsals.size == 0)
    assert(missing(1).forRehearsals.size == 1)
    assert(missing(1).forRehearsals(0).members.size == 1)
  }
}