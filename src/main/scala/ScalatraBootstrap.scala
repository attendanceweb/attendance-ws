import attendance.absence.AbsenceServlet
import attendance.api._
import attendance.attendance.AttendanceServlet
import attendance.config.{ConfigServlet, Configuration}
import attendance.db.DatabaseInit
import attendance.member.MembersServlet
import attendance.rehearsal.RehearsalsServlet
import attendance.report.ReportServlet
import attendance.section.{SectionContactsServlet, SectionsServlet}
import javax.servlet.ServletContext
import org.scalatra._
import org.slf4j.LoggerFactory

class ScalatraBootstrap extends LifeCycle with DatabaseInit {
  private val log = LoggerFactory.getLogger(getClass.getName)

  override def init(context: ServletContext) {
    Configuration.loadDevConfIfExists()

    val url = Configuration.db.url
    val username = Configuration.db.username
    val password = Configuration.db.password

    val database = initDatabase("default", url, username, password)
    if (database.isEmpty) {
      log.error("Unable to connect to database, exiting")
      System.exit(1)
    }

    log.info("Applying migrations")
    migrate(url, username, password).map { _ =>
      log.info("Done applying migrations")
      context.mount(new AbsenceServlet(database.get), "/api/absence/*")
      context.mount(new AttendanceServlet(database.get), "/api/attendance/*")
      context.mount(new ConfigServlet(database.get), "/api/config/*")
      context.mount(new MembersServlet(database.get), "/api/members/*")
      context.mount(new RehearsalsServlet(database.get), "/api/rehearsals/*")
      context.mount(new ReportServlet(database.get), "/api/report/*")
      context.mount(new SectionsServlet(database.get), "/api/sections/*")
      context.mount(new SectionContactsServlet(database.get), "/api/sectioncontacts/*")
      context.initParameters("org.scalatra.environment") = Configuration.env
      context.initParameters("org.scalatra.cors.allowedOrigins") = Configuration.security.allowedOrigins
      context.initParameters("org.scalatra.cors.allowedMethods") = "GET,POST,PUT,DELETE"
    }.recover {
      case _ =>
        log.error("Database migration(s) failed, exiting")
        System.exit(2)
    }
  }
}
