package attendance.project

import java.time.LocalDateTime

import anorm._
import attendance.db.ConnectionPool
import attendance.util.TryWith
import attendance.util.datetime.DateUtil.Interval

class ProjectProvider(implicit db: ConnectionPool) {

  private case class ProjectDb(id: Int, name: String)

  private case class ProjectStartEndDb(project_id: Int, date_start: LocalDateTime, date_end: LocalDateTime)

  def all(): List[Project] = {
    projectsDb.map(project => {
      val startEnd = projectsStartEndDb
        .filter(pse => pse.project_id == project.id)
        .map(pse => Interval(pse.date_start, pse.date_end))
      Project(project.id, project.name, startEnd)
    })
  }

  private def projectsDb: List[ProjectDb] = {
    val parser: RowParser[ProjectDb] = Macro.namedParser[ProjectDb]
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM project""".as(parser.*)
    }.get
  }

  private def projectsStartEndDb: List[ProjectStartEndDb] = {
    val parser: RowParser[ProjectStartEndDb] = Macro.namedParser[ProjectStartEndDb]
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT *
          FROM project_start_end
          ORDER BY date_start ASC
        """.as(parser.*)
    }.get
  }
}