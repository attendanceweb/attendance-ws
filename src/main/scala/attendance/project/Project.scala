package attendance.project

import attendance.util.datetime.DateUtil.Interval

case class Project(id: Int, name: String, startEnd: List[Interval])
