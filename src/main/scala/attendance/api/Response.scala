package attendance.api

case class Response(status: Int, message: String)