package attendance.api

import java.io.{PrintWriter, StringWriter}

import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{CorsSupport, ScalatraServlet}
import org.slf4j.{Logger, LoggerFactory}

trait RestApi extends ScalatraServlet with CorsSupport with JacksonJsonSupport {
  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  notFound {
    contentType = "text/plain"
    response.setStatus(404)
    "Not Found"
  }

  error {
    case ex: Throwable =>
      logger.error("Caught exception: " + getStackTraceAsString(ex))
      contentType = "text/plain"
      response.setStatus(500)
      "Internal Server Error"
  }

  private def getStackTraceAsString(t: Throwable) = {
    val sw = new StringWriter()
    t.printStackTrace(new PrintWriter(sw))
    sw.toString
  }

  options("/*") {
    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"))
  }

  before() {
    contentType = formats("json")
  }
}
