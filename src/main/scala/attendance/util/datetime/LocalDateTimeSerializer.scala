package attendance.util.datetime

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.json4s.CustomSerializer
import org.json4s.JsonAST.JString

case object LocalDateTimeSerializer extends CustomSerializer[LocalDateTime](format => ( {
  case JString(s) => LocalDateTime.parse(s, DateTimeFormatter.ISO_DATE_TIME)
}, {
  case localDateTime: LocalDateTime => JString(localDateTime.format(DateTimeFormatter.ISO_DATE_TIME))
}))
