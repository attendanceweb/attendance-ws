package attendance.util.datetime

import java.time.format.DateTimeFormatter
import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.util.Date


object DateImplicits {
  implicit val localDateTimeOrdering: Ordering[LocalDateTime] = Ordering.by(_.toEpochSecond(ZoneOffset.UTC))
}

object DateUtil {

  case class Interval(date_start: LocalDateTime, date_end: LocalDateTime)

  val maxStrLocalDateTime = "9999-01-01T00:00:00"
  val maxStrLocalDateTime2 = "9999-01-01 00:00:00"

  val ddMMyyyyHHmm: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
  val ddMMyyyy: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
  val ddMM: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM")

  def maxLocalDateTime: LocalDateTime = localDateTime(maxStrLocalDateTime)

  def localDateTime: LocalDateTime = {
    new Date().toInstant.atZone(ZoneId.systemDefault()).toLocalDateTime
  }

  def localDateTime(str: String): LocalDateTime = {
    LocalDateTime.parse(str, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
  }
  
  def localDateTimeToString(localDateTime: LocalDateTime): String = {
    localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
  }

  def dateNextSemesterStart(): LocalDateTime = localDateTime(dateStrNextSemesterStart(localDateTime))

  def dateNextSemesterStart(date: LocalDateTime): LocalDateTime = localDateTime(dateStrNextSemesterStart(date))

  def dateStrNextSemesterStart(): String = dateStrNextSemesterStart(localDateTime)

  def dateStrNextSemesterStart(date: LocalDateTime): String = {
    val year = date.getYear
    if (springSemester(date)) {
      dateStrFallSemesterStart(year)
    } else {
      dateStrSpringSemesterStart(year + 1)
    }
  }

  def dateSemesterStart(): LocalDateTime = localDateTime(dateStrSemesterStart())

  def dateStrSemesterStart(): String = dateStrSemesterStart(localDateTime)

  def dateSemesterStart(date: LocalDateTime): LocalDateTime = localDateTime(dateStrSemesterStart(date))

  def dateStrSemesterStart(date: LocalDateTime): String = {
    val year = date.getYear
    if (springSemester(date)) {
      dateStrSpringSemesterStart(year)
    } else {
      dateStrFallSemesterStart(year)
    }
  }

  def springSemester(): Boolean = springSemester(localDateTime)

  def springSemester(date: LocalDateTime): Boolean = {
    date.getMonthValue >= 1 && date.getMonthValue < 8
  }

  private def dateStrFallSemesterStart(year: Int): String = {
    year + "-08-01T00:00:00"
  }

  private def dateStrSpringSemesterStart(year: Int): String = {
    year + "-01-01T00:00:00"
  }
}