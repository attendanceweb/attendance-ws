package attendance.util

import scala.util.{Success, Try}

object FoldUtil {

  def foldMultipleUntilFailure[T, R](foldables: (() => Seq[Try[T]])*): Try[Boolean] = {
    foldables.foldLeft[Option[Try[T]]](None) {
      (result, fold) => result.orElse(fold().find(_.isFailure))
    }.map(_.map(_ => false)).getOrElse(Success(true))
  }

  def foldUntilFailure[T, R](elements: Seq[T])(f: T => Try[R]): Seq[Try[R]] = {
    foldWhile[T, Try[R]](elements, _.isSuccess)(f)
  }

  def foldWhile[T, R](elements: Seq[T], predicate: R => Boolean)(f: T => R): Seq[R] = {
    elements.foldLeft (Seq.empty[R]) {
      (processed, element) =>
        import attendance.util.RichBoolean._

        def insert(): Option[R] = processed.forall(predicate).collect {
          case true => f(element)
        }

        processed ++ insert()
    }
  }
}
