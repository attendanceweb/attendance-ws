package attendance.util

import java.nio.charset.Charset

import attendance.config.Configuration
import courier.{Envelope, Mailer, Text}
import javax.mail.internet.InternetAddress

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MailUtil {

  def send(to: String, title: String, bodyText: Option[String], bodyHtml: Option[String]): Future[Unit] = {
    val host = Configuration.notification.host
    val port = Configuration.notification.port
    val user = Configuration.notification.user
    val password = Configuration.notification.password
    val from = Configuration.notification.from
    val debug = Configuration.isDevelopment

    val mailer = Mailer(host, port)
      .auth(true)
      .as(user, password)
      .startTtls(true)
      .socketFactory("javax.net.ssl.SSLSocketFactory")
      .debug(debug)()

    val charset = Charset.forName("UTF-8")

    mailer(Envelope.from(new InternetAddress(from))
      .to(new InternetAddress(to))
      .subject(title, charset)
      .content(Text(bodyText.getOrElse(""), charset)))
  }
}
