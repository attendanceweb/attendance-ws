package attendance.util

class RichBoolean(value: Boolean) {
  def collect[B](pf: PartialFunction[Boolean, B]): Option[B] = Option(value).collect(pf)
}

object RichBoolean {
  implicit def richBoolean(value: Boolean): RichBoolean = new RichBoolean(value)
}
