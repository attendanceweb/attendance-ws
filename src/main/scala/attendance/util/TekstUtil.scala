package attendance.util

object TekstUtil {
  
  def toIntTuple(input: String): (Option[Int], Option[Int]) = {
    val elements = input.split("/")
    val first = if (elements.length > 0) Some(elements(0).toInt) else None
    val second = if (elements.length > 1) Some(elements(1).toInt) else None
    (first, second)
  }
  
}