package attendance.member

import java.time.LocalDateTime

import anorm.SqlParser.scalar
import anorm._
import attendance.attendance.AttendanceProvider
import attendance.db.ConnectionPool
import attendance.util.TryWith
import attendance.util.datetime.DateUtil
import attendance.util.datetime.DateUtil._
import org.apache.commons.lang3.exception.ExceptionUtils
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

class MemberProvider(implicit db: ConnectionPool) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  val memberAttendanceProvider = new AttendanceProvider()

  val parser: RowParser[Member] = Macro.namedParser[Member]
  val activityParser: RowParser[MemberActive] = Macro.namedParser[MemberActive]

  def findAll(): List[Member] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id,
            first_name,
            last_name,
            section_id,
            member_active.date_from as date_from,
            member_active.date_to as date_to,
            date_changed
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
        """.as(parser.*)
    }.recover {
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        throw t
    }.get
  }

  def findAllActiveBySectionContactInSemester(sectionContactMemberId: Long): List[Member] = {
    findActiveBySectionContact(sectionContactMemberId, dateSemesterStart(), dateNextSemesterStart())
  }

  def findActiveBySectionContact(sectionContactMemberId: Long, activeFrom: LocalDateTime, activeTo: LocalDateTime): List[Member] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id,
            first_name,
            last_name,
            section_id,
            member_active.date_from as date_from,
            member_active.date_to as date_to,
            date_changed
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          WHERE member.date_changed IS NULL
            AND section_id IN (SELECT section_id FROM section_contact WHERE member_id = $sectionContactMemberId)
            AND date_from < $activeTo
            AND COALESCE(date_to, TO_TIMESTAMP($maxStrLocalDateTime2, 'YYYY-MM-DD HH24:MI:SS')) >= $activeFrom
        """.as(parser.*)
    }.get
  }

  def findInactiveForOverOneYear(): List[Member] = {
    val membersWithDuplicates = TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id,
            first_name,
            last_name,
            section_id,
            member_active.date_from as date_from,
            member_active.date_to as date_to,
            date_changed
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          WHERE
            (SELECT COUNT(*)
             FROM member_active ma
             WHERE ma.member_id = member.member_id
               AND COALESCE(date_to, NOW()) > NOW() - INTERVAL '1 YEARS') = 0
        """.as(parser.*)
    }.get
    membersWithDuplicates.groupBy(m => m.member_id).map(entry => entry._2.last).toList.sortBy(m => m.member_id)
  }

  def findActive(when: LocalDateTime): List[Member] = {
    findActive(when, when)
  }

  def findActive(from: LocalDateTime, to: LocalDateTime): List[Member] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id,
            first_name,
            last_name,
            section_id,
            member_active.date_from as date_from,
            member_active.date_to as date_to,
            date_changed
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          WHERE NOT (date_from > $to OR COALESCE(date_to, TO_TIMESTAMP($maxStrLocalDateTime2, 'YYYY-MM-DD HH24:MI:SS')) < $from)
            AND member.date_changed IS NULL
        """.as(parser.*)
    }.get
  }

  def findById(member_id: Long): Option[Member] = {
    val members = TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id,
            first_name,
            last_name,
            section_id,
            member_active.date_from as date_from,
            member_active.date_to as date_to,
            date_changed
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          WHERE member.member_id = $member_id
            AND member.date_changed IS NULL
        """.as(parser.*)
    }.get
    if (members.isEmpty) None else members.headOption
  }

  def modify(memberId: Long, firstName: String, lastName: String, sectionId: Long, dateChanged: LocalDateTime): Try[Option[Long]] = {
    add(memberId, firstName, lastName, sectionId).map { oldIdOpt =>
      logger.info(s"Added a new member entry for member_id=$memberId.")
      setDateChangedOnOldMemberEntry(memberId, oldIdOpt.getOrElse(0), dateChanged).map { updatedCount =>
        if (updatedCount > 0) {
          logger.info("Sat modification date of the old member entry")
          oldIdOpt
        } else {
          logger.error(s"Unable to set date_changed for member_id=$memberId. Not creating a new member entry.")
          None
        }
      }
    }.flatten
  }

  def add(member_id: Long, firstName: String, lastName: String, sectionId: Long): Try[Option[Long]] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          INSERT INTO member (member_id, first_name, last_name, section_id)
          VALUES ($member_id, $firstName, $lastName, $sectionId)
        """.executeInsert()
    }
  }

  private def setDateChangedOnOldMemberEntry(memberId: Long, oldId: Long, dateChanged: LocalDateTime): Try[Int] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
        UPDATE member SET date_changed=$dateChanged
        WHERE member_id=$memberId AND date_changed IS NULL AND id <> $oldId
      """.executeUpdate() }
  }

  def removeAll(): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM member_active""".executeUpdate()
      SQL"""DELETE FROM member""".executeUpdate()
    }.get
  }

  def remove(memberId: Long): Try[Int] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM member_active WHERE member_id=$memberId""".executeUpdate()
      SQL"""DELETE FROM member WHERE member_id=$memberId""".executeUpdate()
    }
  }

  def nextMemberId(): Try[Long] = maxMemberId().map(max => max)

  def maxMemberId(): Try[Long] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT member_id FROM member
          ORDER BY member_id DESC
          LIMIT 1
        """.as(scalar[Long].singleOpt)
    }.map { maxMemberId =>
      maxMemberId.getOrElse(0L)
    }
  }

  def markAsChanged(member_id: Long, firstName: String, lastName: String, sectionId: Long): Option[Long] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          INSERT INTO member (member_id, first_name, last_name, section_id)
          VALUES ($member_id, $firstName, $lastName, $sectionId)
        """.executeInsert()
    }.get
  }

  def isActive(member_id: Long): Boolean = findIfActive(member_id).map(_.nonEmpty).get

/*  def all(): List[MemberActive] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM member_active""".as(parser.*)
    }.get
  }*/

  def findIfActive(memberId: Long): Try[Option[MemberActive]] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM member_active
        WHERE member_id=$memberId AND date_to IS NULL""".as(activityParser.*)
    }.map(_.headOption)
  }

  def activityAt(memberId: Long, date: LocalDateTime): Option[MemberActive] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM member_active
        WHERE member_id=$memberId AND $date >= date_from AND $date < COALESCE(date_to, TO_TIMESTAMP(${DateUtil.maxStrLocalDateTime2}, 'YYYY-MM-DD HH24:MI:SS'))
        ORDER BY date_from DESC, COALESCE(date_to, TO_TIMESTAMP(${DateUtil.maxStrLocalDateTime2}, 'YYYY-MM-DD HH24:MI:SS')) ASC""".as(activityParser.*)
    }.get.headOption
  }

  // TODO remove.. it shouldn't be possible to add closed activity periods
  def addActivity(memberId: Long, dateFrom: LocalDateTime, dateTo: Option[LocalDateTime]): Option[Long] = {
    if (isActive(memberId)) {
      None
    } else {
      TryWith(db.connection.get) { implicit con =>
        SQL"""INSERT INTO member_active (member_id, date_from, date_to)
             VALUES ($memberId, $dateFrom, $dateTo)""".executeInsert()
      }.get
    }
  }

  def activate(member_id: Long, date_from: LocalDateTime): MemberActivateStatus.Value = {
    if (isActive(member_id)) {
      MemberActivateStatus.AlreadyActive
    } else {
      TryWith(db.connection.get) { implicit con =>
        val res: Option[Long] = SQL"""INSERT INTO member_active (member_id, date_from)
          VALUES ($member_id, $date_from)""".executeInsert()
        if (res.getOrElse(0L) > 0) MemberActivateStatus.Activated else MemberActivateStatus.AlreadyActive
      }.recover {
        case t: Throwable =>
          logger.error(ExceptionUtils.getStackTrace(t))
          MemberActivateStatus.DatabaseError
      }.get
    }
  }

  def deactivate(member_id: Long, date_to: LocalDateTime): MemberActivateStatus.Value = {
    activityAt(member_id, date_to).map { activity =>
      activity.date_to.map(_ => MemberActivateStatus.AlreadyInactive).getOrElse {
        TryWith(db.connection.get) { implicit con =>
          SQL"""
            UPDATE member_active
            SET date_to = $date_to
            WHERE id = ${activity.id}
          """.executeUpdate()
        }.map { count =>
          if (count > 0) MemberActivateStatus.Deactivated else MemberActivateStatus.DatabaseError
        }.recover {
          case t: Throwable =>
            logger.error(ExceptionUtils.getStackTrace(t))
            MemberActivateStatus.DatabaseError
        }.get
      }
    }.getOrElse(MemberActivateStatus.AlreadyInactive)
  }

  def modify(memberId: Long, dateFrom: LocalDateTime): MemberActivateStatus.Value = {
    def doModify(id: Long, dateFrom: LocalDateTime): Try[Int] = {
      TryWith(db.connection.get) { implicit con =>
        SQL"""
          UPDATE member_active
          SET date_from = $dateFrom
          WHERE id = $id
        """.executeUpdate()
      }
    }

    findIfActive(memberId).map { currentActivityOpt =>
      currentActivityOpt.map { currentActivity =>
        if (dateFrom.isBefore(currentActivity.date_from)) {
          activityAt(memberId, dateFrom).map { _ =>
            MemberActivateStatus.OverlappingActivityPeriods
          }.getOrElse {
            doModify(currentActivity.id, dateFrom).map { count =>
              if (count > 0) MemberActivateStatus.ChangedActivityPeriod else MemberActivateStatus.NotChangedActivityPeriod
            }.recover {
              case t: Throwable =>
                logger.error(ExceptionUtils.getStackTrace(t))
                MemberActivateStatus.DatabaseError
            }.get
          }
        } else {
          val attendance = memberAttendanceProvider.between(memberId, currentActivity.date_from, dateFrom)
          if (attendance.nonEmpty) {
            MemberActivateStatus.ChangeWouldOrphanAttendance
          } else {
            doModify(currentActivity.id, dateFrom).map { count =>
              if (count > 0) MemberActivateStatus.ChangedActivityPeriod else MemberActivateStatus.NotChangedActivityPeriod
            }.recover {
              case t: Throwable =>
                logger.error(ExceptionUtils.getStackTrace(t))
                MemberActivateStatus.DatabaseError
            }.get
          }
        }
      }.getOrElse {
        MemberActivateStatus.ActivityNotFound
      }
    }.recover {
      // TODO restructure error handling
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        MemberActivateStatus.DatabaseError
    }.get
  }
}
