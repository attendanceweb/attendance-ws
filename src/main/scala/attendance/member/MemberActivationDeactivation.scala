package attendance.member

import java.time.LocalDateTime

import org.slf4j.LoggerFactory

object MemberActivateStatus extends Enumeration {
  val Activated, OverlappingActivityPeriods, AlreadyActive,
  Deactivated, AlreadyInactive,
  ChangedActivityPeriod, NotChangedActivityPeriod, ChangeWouldOrphanAttendance, ActivityNotFound,
  MemberNotFound,
  DatabaseError = Value
}

case class MemberActivateRequest(date_from: LocalDateTime)
case class MemberDeactivateRequest(date_to: LocalDateTime)
case class MemberActivateDeactivateResponse(status: MemberActivateStatus.Value, message: String)

class MemberActivationDeactivation(memberProvider: MemberProvider) {

  private val logger = LoggerFactory.getLogger(getClass.getName)

  def activate(memberId: Long, activateRequest: MemberActivateRequest): MemberActivateDeactivateResponse = {
    val status = memberProvider.activate(memberId, activateRequest.date_from)
    logger.info("member_id=" + memberId + " status=" + status)
    MemberActivateDeactivateResponse(status, "")
  }

  def deactivate(memberId: Long, deactivateRequest: MemberDeactivateRequest): MemberActivateDeactivateResponse = {
    val status = memberProvider.deactivate(memberId, deactivateRequest.date_to)
    logger.info("member_id=" + memberId + " status=" + status)
    MemberActivateDeactivateResponse(status, "")
  }
}
