package attendance.member

import java.time.LocalDateTime

import attendance.util.datetime.DateUtil

case class Member(
                   member_id: Int,
                   first_name: String,
                   last_name: String,
                   section_id: Int,
                   date_from: LocalDateTime,
                   date_to: Option[LocalDateTime],
                   date_changed: Option[LocalDateTime]
                 ) {

  def isActive: Boolean = date_to.isEmpty

  def isActive(dateFrom: LocalDateTime, dateTo: LocalDateTime): Boolean = {
    val date_to_or_default = date_to.getOrElse(DateUtil.maxLocalDateTime)
    !dateTo.isBefore(date_from) && !dateFrom.isAfter(date_to_or_default) && dateFrom != date_to_or_default
  }
}
