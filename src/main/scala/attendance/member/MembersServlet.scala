package attendance.member

import attendance.api.{Response, RestApi}
import attendance.attendance.AttendanceProvider
import attendance.db.ConnectionPool
import attendance.util.datetime.{LocalDateSerializer, LocalDateTimeSerializer}
import javax.servlet.http.HttpServletRequest
import org.apache.commons.lang3.exception.ExceptionUtils
import org.json4s.ext.EnumNameSerializer
import org.json4s.{DefaultFormats, Formats}
import org.scalatra._

import scala.util.Try

class MembersServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats +
    LocalDateSerializer + LocalDateTimeSerializer +
    new EnumNameSerializer(MemberActivateStatus) +
    new EnumNameSerializer(MemberModificationStatus)

  implicit val database: ConnectionPool = db
  val memberProvider = new MemberProvider()
  val memberActivationDeactivation = new MemberActivationDeactivation(memberProvider)
  val memberModification = new MemberModification(memberProvider)
  val attendanceProvider = new AttendanceProvider()

  get("/") {
    logger.info("Get members")
    memberProvider.findAll()
  }

  get("/:id") {
    val id = params.getOrElse("id", halt(400)).toLong
    logger.info("Get member id=" + id)
    List(memberProvider.findById(id))
  }

  post("/") {
    logger.info("Add member")
    Try(readJsonFromBody[Member](request)).map { member =>
      memberProvider.findById(member.member_id).map { existingMember =>
        BadRequest(Response(1, s"Member ${existingMember.member_id} already exists"))
      }.getOrElse {
        memberProvider.add(member.member_id, member.first_name, member.last_name, member.section_id).map { _ =>
          logger.info("Added member id=" + member.member_id)
          val result = memberProvider.activate(member.member_id, member.date_from)
          if (result == MemberActivateStatus.Activated) {
            logger.info("Added member activity period for id=" + member.member_id + " date_from=" + member.date_from + " date_to=" + member.date_to)
            Ok(Response(0, "Added member"))
          } else {
            logger.error("Failed to add member: " + result)
            InternalServerError()
          }
        }.recover {
          case t: Throwable =>
            logger.error(ExceptionUtils.getStackTrace(t))
            InternalServerError()
        }.get
      }
      // TODO handle exceptions in SQL code
      /*.recover {
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        InternalServerError()
    }.get*/
    }.recover {
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        BadRequest()
    }.get
  }

  put("/:id") {
    val memberId = params.getOrElse("id", halt(400)).toLong
    logger.info("Modify member id " + memberId)
    Try(readJsonFromBody[MemberModifyRequest](request)).map { memberModifyRequest =>
      val modifyResponse = memberModification.modify(memberId, memberModifyRequest)
      modifyResponse.status match {
        case MemberModificationStatus.AddingFailed => InternalServerError(modifyResponse)
        case MemberModificationStatus.Ok => Ok(modifyResponse)
        case _ => BadRequest(modifyResponse)
      }
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  private def readJsonFromBody[T: Manifest](request: HttpServletRequest): T =
    parse(request.body).extract[T]

  put("/activate/:id") {
    val memberId = params.getOrElse("id", halt(400)).toLong
    logger.info("Activate member id " + memberId)
    Try(readJsonFromBody[MemberActivateRequest](request)).map { memberActivateRequest =>
      memberActivationDeactivation.activate(memberId, memberActivateRequest)
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  put("/deactivate/:id") {
    val memberId = params.getOrElse("id", halt(400)).toLong
    logger.info("Deactivate member id " + memberId)
    Try(readJsonFromBody[MemberDeactivateRequest](request)).map { memberDeactivateRequest =>
      memberActivationDeactivation.deactivate(memberId, memberDeactivateRequest)
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  get("/cleanup") {
    logger.info("Get a list of members that have been inactive for over one year")
    memberProvider.findInactiveForOverOneYear()
  }

  post("/cleanup") {
    logger.info("Remove members that have been inactive for over one year")
    val members = memberProvider.findInactiveForOverOneYear()
    members.foreach { member =>
      logger.info(s"Removing attendance for member ${member.member_id}")
      attendanceProvider.remove(member.member_id)
      logger.info(s"Removing member ${member.member_id}")
      memberProvider.remove(member.member_id)
    }
    Ok(Response(0, "Removed"))
  }
}
