package attendance.member

import java.time.LocalDateTime

case class MemberActive(
                         id: Long,
                         member_id: Long,
                         date_from: LocalDateTime,
                         date_to: Option[LocalDateTime]
                       )
