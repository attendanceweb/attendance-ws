package attendance.member

import attendance.util.datetime.DateUtil.localDateTime
import org.apache.commons.lang3.exception.ExceptionUtils
import org.slf4j.{Logger, LoggerFactory}

object MemberModificationStatus extends Enumeration {
  val Ok, MemberNotFound, NoChanges, AddingFailed = Value
}

case class MemberModifyRequest(first_name: String, last_name: String, section_id: Long)

case class MemberModifyResponse(status: MemberModificationStatus.Value, message: String)

class MemberModification(memberProvider: MemberProvider) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  def modify(memberId: Long, modifyRequest: MemberModifyRequest): MemberModifyResponse = {
    memberProvider.findById(memberId).map { oldMember =>
      if (!isDifferent(oldMember, modifyRequest)) {
        val msg = "No changes detected"
        logger.info(msg)
        MemberModifyResponse(MemberModificationStatus.NoChanges, msg)
      } else {
        logger.info("Modifying old member: " + oldMember + " with data: " + modifyRequest)
        memberProvider.modify(oldMember.member_id, modifyRequest.first_name, modifyRequest.last_name, modifyRequest.section_id, localDateTime).map { _ =>
          val msg = "Modified member"
          logger.info(msg)
          MemberModifyResponse(MemberModificationStatus.Ok, msg)
        }.recover {
          case t: Throwable =>
            val msg = "Failed to modify member"
            logger.info(msg)
            logger.error(ExceptionUtils.getStackTrace(t))
            MemberModifyResponse(MemberModificationStatus.AddingFailed, msg)
        }.get
      }
    }.getOrElse {
      val msg = "Member id " + memberId + " not found"
      logger.info(msg)
      MemberModifyResponse(MemberModificationStatus.MemberNotFound, msg)
    }
  }

  def isDifferent(member: Member, request: MemberModifyRequest): Boolean = {
    member.first_name != request.first_name ||
      member.last_name != request.last_name ||
      member.section_id != request.section_id
  }
}
