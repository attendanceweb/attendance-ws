package attendance.attendance

import java.io.OutputStream

import attendance.api.{Response, RestApi}
import attendance.db.ConnectionPool
import attendance.member._
import attendance.rehearsal.{Rehearsal, RehearsalProvider}
import attendance.util.FoldUtil._
import attendance.util.datetime.DateUtil._
import attendance.util.datetime.{DateImplicits, LocalDateSerializer, LocalDateTimeSerializer}
import com.github.tototoshi.csv.CSVWriter
import org.apache.commons.lang3.exception.ExceptionUtils
import org.json4s.{DefaultFormats, Formats, _}
import org.scalatra._

import scala.collection.mutable.ListBuffer
import scala.util.{Success, Try}

class AttendanceServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val memberProvider = new MemberProvider()
  val memberAttendanceProvider = new AttendanceProvider()
  val rehearsalProvider = new RehearsalProvider()

  get("/:rehearsalId.csv") {
    Try {
      val rehearsalId = params.getOrElse("rehearsalId", halt(400)).toInt
      logger.info("Get attendance in semester by rehearsal " + rehearsalId)
      contentType = "text/csv"
      rehearsalProvider.closestTo(localDateTime).map { rehearsal =>
        attendanceInSemesterAsCsv(rehearsal, response.outputStream)
        Ok()
      }.getOrElse {
        logger.info("Rehearsal not found")
        NotFound()
      }
    }.recover {
      case _: NumberFormatException =>
        logger.info("Invalid rehearsal ID")
        BadRequest()
    }.get
  }

  private def attendanceInSemesterAsCsv(rehearsal: Rehearsal, outputStream: OutputStream): Unit = {
    val dateStart = dateSemesterStart(rehearsal.date_start)
    val dateEnd = dateNextSemesterStart(rehearsal.date_start)

    val members = memberProvider.findActive(dateStart, dateEnd).sortBy(m => m.first_name + m.last_name)
    val attendance = memberAttendanceProvider.between(dateStart, dateEnd)

    val rehearsals = rehearsalProvider.between(dateStart, dateEnd).sortBy(_.date_start)(DateImplicits.localDateTimeOrdering)

    val csvWriter = CSVWriter.open(outputStream, "UTF-8")
    val csvFieldNames = List("Rehearsal date", "First name", "Last name", "Attendance")
    val csvRehearsalDelimiter = List("", "", "", "")

    csvWriter.writeRow(csvFieldNames)

    def attendanceFields(rehearsal: Rehearsal, attendance: AttendanceForRehearsal): List[List[String]] = {
      attendance.members.sortBy(m => m.first_name).map { m =>
        val presentOpt = attendance.for_members.find(p => p.member_id == m.member_id).map(p => AttendanceType.parseInt(p.present))
        val dateStart = rehearsal.date_start.format(ddMMyyyyHHmm)
        List(dateStart, m.first_name, m.last_name, presentOpt.getOrElse(AttendanceType.Unspecified).toString)
      }
    }

    def rehearsalAttendanceFields(rehearsal: Rehearsal): List[List[String]] = {
      val activeMembersFiltered: List[Member] = members.filter(m => m.isActive(rehearsal.date_start, rehearsal.date_end))
      val attendanceFiltered: List[Attendance] = attendance.filter(a => a.rehearsal_id == rehearsal.id)
      val attendanceForRehearsal = buildAttendanceForRehearsal(rehearsal, activeMembersFiltered, attendanceFiltered)
      attendanceFields(rehearsal, attendanceForRehearsal)
    }

    rehearsals.foreach { rehearsal =>
      rehearsalAttendanceFields(rehearsal).foreach(field => csvWriter.writeRow(field))
      csvWriter.writeRow(csvRehearsalDelimiter)
    }
  }

  get("//:rehearsalId") {
    Try {
      val rehearsalId = params.getOrElse("rehearsalId", halt(400)).toInt
      logger.info("Get attendance for rehearsal " + rehearsalId)
      List(getAttendance(None, Some(rehearsalId)))
    }.recover {
      case _: NumberFormatException =>
        logger.info("Invalid rehearsal ID")
        BadRequest()
    }.get
  }

  get("/:sectionContactId/:rehearsalId") {
    Try {
      val rehearsalId = params.getOrElse("rehearsalId", halt(400)).toInt
      val sectionContactId = params.getOrElse("sectionContactId", halt(400)).toInt
      logger.info("Get attendance for section contact " + sectionContactId + " and rehearsal " + rehearsalId)
      List(getAttendance(Some(sectionContactId), Some(rehearsalId)))
    }.recover {
      case _: NumberFormatException =>
        logger.info("Invalid rehearsal ID or section contact ID")
        BadRequest()
    }.get
  }

  private def getAttendance(sectionContactMemberIdOpt: Option[Int], rehearsalId: Option[Int]): Option[AttendanceForRehearsal] = {
    val rehearsalOpt = if (rehearsalId.isEmpty) rehearsalProvider.closestTo(localDateTime) else rehearsalProvider.byId(rehearsalId.get)
    rehearsalOpt.map { rehearsal =>
      logger.info("Rehearsal id=" + rehearsal.id)
      Some(findAttendanceBySectionContactAndRehearsal(sectionContactMemberIdOpt, rehearsal))
    }.getOrElse {
      logger.info("Rehearsal not found")
      None
    }
  }

  private def findAttendanceBySectionContactAndRehearsal(sectionContactMemberId: Option[Int], rehearsal: Rehearsal): AttendanceForRehearsal = {
    val members = sectionContactMemberId.map { id =>
      memberProvider.findActiveBySectionContact(id, rehearsal.date_start, rehearsal.date_end)
    }.getOrElse {
      memberProvider.findActive(rehearsal.date_start, rehearsal.date_end)
    }
    val attendance = sectionContactMemberId.map { id =>
      memberAttendanceProvider.findBySectionContactAndRehearsal(id, rehearsal)
    }.getOrElse {
      memberAttendanceProvider.findByRehearsal(rehearsal)
    }
    buildAttendanceForRehearsal(rehearsal, members, attendance)
  }

  private def buildAttendanceForRehearsal(rehearsal: Rehearsal, members: List[Member], attendance: List[Attendance]): AttendanceForRehearsal = {
    var memberAttendanceWithMissing = new ListBuffer[AttendanceForMember]
    members.sortBy(m => m.first_name).foreach { m =>
      val maybeAttendance: Option[Attendance] = attendance.find(a => a.member_id == m.member_id)
      // TODO use AttendanceType
      val present: Int = maybeAttendance.map(_.present).getOrElse(-1)
      memberAttendanceWithMissing += AttendanceForMember(m.member_id, present)
    }
    AttendanceForRehearsal(rehearsal.id, members, memberAttendanceWithMissing.toList)
  }

  post("/:sectionContactId/:rehearsalId") {
    val sectionContactId = params.getOrElse("sectionContactId", halt(400)).toInt
    val rehearsalId = params.getOrElse("rehearsalId", halt(400)).toInt
    Try {
      val json = parse(request.body)
      json.extract[AttendanceForRehearsal]
    }.map { attendanceForRehearsal =>
      logger.info("Update attendance for section contact " + sectionContactId + " and rehearsal " + rehearsalId)
      saveAttendance(attendanceForRehearsal).map { result =>
        if (result) {
          Ok(Response(0, "Saved attendance"))
        } else {
          logger.error("Failed to update attendance")
          InternalServerError()
        }
      }.recover {
        case t: Throwable =>
          logger.error(ExceptionUtils.getStackTrace(t))
          InternalServerError()
      }.get
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  private def saveAttendance(attendanceForRehearsal: AttendanceForRehearsal): Try[Boolean] = {
    val foldables: () => Seq[Try[Boolean]] = () => foldUntilFailure(attendanceForRehearsal.for_members) { attendance =>
      if (attendance.present != -1) {
        logger.info("Deleting (if exists) and inserting attendance " + attendance + " for rehearsal " + attendanceForRehearsal.rehearsal_id)
        memberAttendanceProvider.deleteAndInsert(attendanceForRehearsal.rehearsal_id, attendance)
      } else Success(true)
    }
    foldMultipleUntilFailure(foldables)
  }
}
