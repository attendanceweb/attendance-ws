package attendance.attendance

import attendance.member.Member

case class AttendanceForRehearsal(
    rehearsal_id: Int,
    members: List[Member],
    for_members: List[AttendanceForMember]
)

case class AttendanceForMember(
    member_id: Int,
    present: Int
)