package attendance.attendance

import java.time.LocalDateTime

import anorm._
import attendance.db.ConnectionPool
import attendance.rehearsal.Rehearsal
import attendance.util.TryWith

import scala.util.Try

class AttendanceProvider(implicit db: ConnectionPool) {

  val parser: RowParser[Attendance] = Macro.namedParser[Attendance]

  def between(startDate: LocalDateTime, endDate: LocalDateTime): List[Attendance] = {
    val result = TryWith(db.connection.get) { implicit con =>
      SQL"""
        SELECT
          member.member_id AS member_id,
          first_name,
          last_name,
          section_id,
          member_attendance.rehearsal_id,
          rehearsal.date_start AS rehearsal_date_start,
          rehearsal.date_end AS rehearsal_date_end,
          member_attendance.present
        FROM member
        JOIN member_attendance ON member_attendance.member_id = member.member_id
        JOIN rehearsal ON rehearsal.id = member_attendance.rehearsal_id
        WHERE rehearsal.date_start >= $startDate
          AND rehearsal.date_end <= $endDate
      """.as(parser.*)
    }.get
    filterDuplicateMembers(result)
  }

  private def filterDuplicateMembers(result: List[Attendance]): List[Attendance] =
    result.groupBy(_.rehearsal_id).flatMap { forRehearsal =>
      forRehearsal._2.groupBy(_.member_id).map(_._2.head)
    }.toList

  def between(memberId: Long, startDate: LocalDateTime, endDate: LocalDateTime): List[Attendance] = {
    val result = TryWith(db.connection.get) { implicit con =>
      SQL"""
        SELECT
          member.member_id AS member_id,
          first_name,
          last_name,
          section_id,
          member_attendance.rehearsal_id,
          rehearsal.date_start AS rehearsal_date_start,
          rehearsal.date_end AS rehearsal_date_end,
          member_attendance.present
        FROM member
        JOIN member_attendance ON member_attendance.member_id = member.member_id
        JOIN rehearsal ON rehearsal.id = member_attendance.rehearsal_id
        WHERE rehearsal.date_start >= $startDate
          AND rehearsal.date_end <= $endDate
          AND member.member_id = $memberId
      """.as(parser.*)
    }.get
    filterDuplicateMembers(result)
  }

  def findBySectionContactAndRehearsal(sectionContactId: Long, rehearsal: Rehearsal): List[Attendance] = {
    val result = TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id AS member_id,
            first_name,
            last_name,
            section_id,
            member_attendance.rehearsal_id,
            rehearsal.date_start AS rehearsal_date_start,
            rehearsal.date_end AS rehearsal_date_end,
            member_attendance.present
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          JOIN member_attendance ON member_attendance.member_id = member.member_id
          JOIN rehearsal ON rehearsal.id = member_attendance.rehearsal_id
          WHERE rehearsal_id = ${rehearsal.id}
            AND section_id IN (SELECT section_id FROM section_contact WHERE member_id = $sectionContactId)
            AND member_active.date_from <= ${rehearsal.date_start}
            AND (member_active.date_to IS NULL OR member_active.date_to >= ${rehearsal.date_end})
        """.as(parser.*)
    }.get
    filterDuplicateMembers(result)
  }

  def findByRehearsal(rehearsal: Rehearsal): List[Attendance] = {
    val result = TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id AS member_id,
            first_name,
            last_name,
            section_id,
            member_attendance.rehearsal_id,
            rehearsal.date_start AS rehearsal_date_start,
            rehearsal.date_end AS rehearsal_date_end,
            member_attendance.present
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          JOIN member_attendance ON member_attendance.member_id = member.member_id
          JOIN rehearsal ON rehearsal.id = member_attendance.rehearsal_id
          WHERE rehearsal_id = ${rehearsal.id}
            AND member_active.date_from <= ${rehearsal.date_start}
            AND (member_active.date_to IS NULL OR member_active.date_to >= ${rehearsal.date_end})
        """.as(parser.*)
    }.get
    filterDuplicateMembers(result)
  }

  def all(): List[Attendance] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            member.member_id AS member_id,
            first_name,
            last_name,
            section_id,
            member_attendance.rehearsal_id,
            rehearsal.date_start AS rehearsal_date_start,
            rehearsal.date_end AS rehearsal_date_end,
            member_attendance.present
          FROM member
          JOIN member_active ON member_active.member_id = member.member_id
          JOIN member_attendance ON member_attendance.member_id = member.member_id
          JOIN rehearsal ON rehearsal.id = member_attendance.rehearsal_id
        """.as(parser.*)
    }.get
  }

  def deleteAndInsert(rehearsalId: Long, memberAttendance: AttendanceForMember): Try[Boolean] = {
    remove(memberAttendance.member_id, rehearsalId).map { _ =>
      add(memberAttendance.member_id, rehearsalId, memberAttendance.present).map(_.nonEmpty)
    }.flatten
  }

  def add(memberId: Long, rehearsalId: Long, present: Long): Try[Option[Long]] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
            INSERT INTO member_attendance (
              member_id,
              rehearsal_id,
              present
            ) VALUES (
              $memberId,
              $rehearsalId,
              $present
            )
          """.executeInsert()
    }
  }

  def remove(memberId: Long, rehearsalId: Long): Try[Int] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          DELETE FROM member_attendance
          WHERE rehearsal_id = $rehearsalId
            AND member_id = $memberId
        """.executeUpdate()
    }
  }

  def remove(memberId: Long): Try[Int] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          DELETE FROM member_attendance
          WHERE member_id = $memberId
        """.executeUpdate()
    }
  }

  def removeAll(): Try[Int] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM member_attendance""".executeUpdate()
    }
  }
}
