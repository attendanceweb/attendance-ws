package attendance.attendance

object AttendanceType extends Enumeration {
  val Absent, Present, AbsentNotified, Unspecified = Value

  def parseInt(value: Int) = {
    value match {
      case 0 => Absent
      case 1 => Present
      case 2 => AbsentNotified
      case default => Unspecified
    }
  }

}
