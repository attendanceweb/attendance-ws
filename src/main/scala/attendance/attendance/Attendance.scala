package attendance.attendance

import java.time.LocalDateTime

import attendance.project.Project
import attendance.util.datetime.DateUtil

case class Attendance(
                             member_id: Int,
                             first_name: String,
                             last_name: String,
                             section_id: Int,
                             rehearsal_id: Int,
                             rehearsal_date_start: LocalDateTime,
                             rehearsal_date_end: LocalDateTime,
                             present: Int
                           ) {
  def rehearsalDateStartFormatted: String = rehearsal_date_start.format(DateUtil.ddMM)

  def isInProject(project: Project): Boolean = {
    project.startEnd.exists(startEnd => startEnd.date_start.compareTo(rehearsal_date_start) <= 0 && startEnd.date_end.compareTo(rehearsal_date_end) >= 0)
  }
}
