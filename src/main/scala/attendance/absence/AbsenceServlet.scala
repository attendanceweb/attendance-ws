package attendance.absence

import attendance.api.RestApi
import attendance.config.Configuration
import attendance.db.ConnectionPool
import attendance.util.datetime.DateUtil.{dateStrSemesterStart, localDateTime}
import attendance.util.datetime.{LocalDateSerializer, LocalDateTimeSerializer}
import org.json4s.{DefaultFormats, Formats}

class AbsenceServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val memberAbsenceProvider = new AbsenceProvider()

  get("/") {
    val from = localDateTime(dateStrSemesterStart())
    val to = localDateTime
    logger.info("Get member absence between " + from + " - " + to)
    memberAbsenceProvider.absenceForSectionContactsBetween(Configuration.absenceLimit, from, to)
  }

  get("/:from") {
    val from = localDateTime(params.getOrElse("from", halt(400)))
    val to = localDateTime
    logger.info("Get member absence between " + from + " - " + to)
    memberAbsenceProvider.absenceForSectionContactsBetween(Configuration.absenceLimit, from, to)
  }

  get("/:from/:to") {
    val from = params.getOrElse("from", halt(400))
    val to = params.getOrElse("to", halt(400))
    logger.info("Get member absence between " + from + " - " + to)
    memberAbsenceProvider.absenceForSectionContactsBetween(Configuration.absenceLimit, localDateTime(from), localDateTime(to))
  }
}
