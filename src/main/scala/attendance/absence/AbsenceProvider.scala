package attendance.absence

import java.time.LocalDateTime

import attendance.attendance.{AttendanceType, Attendance, AttendanceProvider}
import attendance.db.ConnectionPool
import attendance.member.{Member, MemberProvider}
import attendance.project.{Project, ProjectProvider}
import attendance.rehearsal.RehearsalProvider
import attendance.section.{SectionContact, SectionContactProvider}
import attendance.util.datetime.DateImplicits
import org.slf4j.{Logger, LoggerFactory}

class AbsenceProvider(implicit db: ConnectionPool) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  val sectionContactProvider = new SectionContactProvider
  val memberProvider = new MemberProvider
  val rehearsalProvider = new RehearsalProvider
  val projectProvider = new ProjectProvider
  val memberAttendanceProvider = new AttendanceProvider

  def absenceForSectionContactsBetween(absenceLimit: Int, from: LocalDateTime, to: LocalDateTime): List[MemberAbsenceForSectionContact] = {
    val activeMembersWholePeriod = activeMembersBetween(from, to)
    val activeMembersAtTheEnd = memberProvider.findActive(to)
    val absence: List[Attendance] = memberAttendanceAbsent(from, to, activeMembersWholePeriod)
    val memberAbsence: List[MemberAbsence] = memberAbsenceList(absenceLimit, activeMembersWholePeriod, activeMembersAtTheEnd, absence)
    memberAbsenceForSectionContacts(absenceLimit, memberAbsence)
  }

  private def activeMembersBetween(from: LocalDateTime, to: LocalDateTime): List[Member] = {
    memberProvider.findActive(from, to).groupBy(_.member_id).map(_._2.head).toList
  }

  private def memberAttendanceAbsent(from: LocalDateTime, to: LocalDateTime, members: List[Member]): List[Attendance] = {
    memberAttendanceProvider
      .between(from, to)
      .filter(a => a.present != AttendanceType.Present.id)
      .filter(a => members.exists(m => m.member_id == a.member_id))
  }

  private def memberAbsenceList(absenceLimit: Int,
                                activeMembersWholePeriod: List[Member],
                                activeMembersAtTheEnd: List[Member],
                                absence: List[Attendance]): List[MemberAbsence] = {
    def isActive(member: Member) = activeMembersAtTheEnd.exists(activeMember => activeMember.member_id == member.member_id)

    def buildMemberAbsence(member: Member, absenceForMember: List[Attendance]): MemberAbsence = {
      val absenceForProjects = buildAbsenceForProjects(absenceForMember)
      MemberAbsence(member.member_id, member.first_name, member.last_name, member.section_id, absenceForMember.size, isActive(member), absenceForProjects)
    }

    activeMembersWholePeriod.flatMap { member =>
      Option(absence.filter(a => a.member_id == member.member_id))
        .filter(a => a.lengthCompare(absenceLimit) >= 0)
        .map(a => buildMemberAbsence(member, a))
    }
  }

  private def buildAbsenceForProjects(memberAbsenceAll: List[Attendance]): List[MemberAbsenceForProject] = {
    val projects: List[Project] = projectProvider.all()

    def forProjects = {
      projects.flatMap { project =>
        val absenceForProject = memberAbsenceAll
          .filter(absence => absence.isInProject(project))
          .sortBy(_.rehearsal_date_start)(DateImplicits.localDateTimeOrdering)
        val absenceForProjectObj = MemberAbsenceForProject(Some(project), absenceForProject)
        Option(absenceForProjectObj).filter(ap => ap.absence.nonEmpty)
      }
    }

    def withoutProjects(absenceForProjects: List[MemberAbsenceForProject]): List[MemberAbsenceForProject] = {
      val absenceInProjects: List[Attendance] = absenceForProjects.flatMap(a => a.absence)
      val absenceWithoutProjects = memberAbsenceAll
        .filter(a => !absenceInProjects.contains(a))
        .sortBy(_.rehearsal_date_start)(DateImplicits.localDateTimeOrdering)
      List(absenceWithoutProjects)
        .filter(a => a.nonEmpty)
        .map(a => MemberAbsenceForProject(None, a))
    }

    val fp = forProjects
    fp ++ withoutProjects(fp)
  }

  private def memberAbsenceForSectionContacts(absenceLimit: Int, memberAbsence: List[MemberAbsence]) = {
    val sectionContacts: List[SectionContact] = sectionContactProvider.all()
    sectionContacts
      .map(sc => memberAbsenceForSectionContact(sc, memberAbsence))
      .sortBy(_.sectionContact.section_id)
  }

  private def memberAbsenceForSectionContact(sectionContact: SectionContact, memberAbsence: List[MemberAbsence]) = {
    val forSectionContact = memberAbsence.filter(ma => sectionContact.sections.exists(_.id == ma.section_id))
    MemberAbsenceForSectionContact(sectionContact, forSectionContact)
  }
}
