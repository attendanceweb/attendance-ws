package attendance.absence

import attendance.attendance.Attendance
import attendance.project.Project
import attendance.section.SectionContact

case class MemberAbsenceForSectionContact(sectionContact: SectionContact,
                                          membersAbsence: List[MemberAbsence])

case class MemberAbsence(member_id: Int,
                          first_name: String,
                          last_name: String,
                          section_id: Int,
                          absence_count: Int,
                          active: Boolean,
                          absence: List[MemberAbsenceForProject]
                        ) {

  def sameMemberAndProjectsAs(o: Any): Boolean = o match {
    case that: MemberAbsence => (that.member_id == this.member_id) && this.sameProjectsAs(that) && that.sameProjectsAs(this)
    case _ => false
  }

  private def sameProjectsAs(other: MemberAbsence): Boolean = {
    for (f <- absence) {
      if (!other.absence.exists(s => s.sameProjectAs(f))) {
        return false
      }
    }
    true
  }
}

case class MemberAbsenceForProject(project: Option[Project],
                                   absence: List[Attendance]) {

  def sameProjectAs(other: MemberAbsenceForProject): Boolean = {
    if (project.isEmpty && other.project.isEmpty || (project.nonEmpty && other.project.nonEmpty && project.get.id == other.project.get.id)) true else false
  }
}