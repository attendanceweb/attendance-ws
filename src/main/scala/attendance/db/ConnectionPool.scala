package attendance.db

import java.sql.{Connection, SQLException}
import java.util.concurrent.TimeUnit
import com.jolbox.bonecp.{BoneCP, BoneCPConfig}
import org.slf4j.LoggerFactory

/*
 * Heavily inspired by https://github.com/satendrakumar/sbt-connectionpool-example
 */
class ConnectionPool(driverName: String, poolName: String, jdbcUrl: String, username: String, password: String) {

  private val log = LoggerFactory.getLogger(getClass.getName)

  def connection: Option[Connection] = {
    connectionPool.map(p => p.getConnection)
  }

  def initialized(): Boolean = {
    connectionPool.isDefined
  }

  def poolInfo(): Unit = {
    println(s"driverName: $driverName, jdbcUrl: $jdbcUrl, username: $username")
  }

  private val connectionPool = {
    try {
      // Load the database driver (make sure this is in your classpath!)
      log.info("Loading database driver " + driverName)
      Class.forName(driverName)

      try {
        val config = new BoneCPConfig
        config.setJdbcUrl(jdbcUrl)
        config.setUsername(username)
        config.setPassword(password)
        config.setMinConnectionsPerPartition(2)
        config.setMaxConnectionsPerPartition(5)
        config.setPartitionCount(3)
        config.setAcquireRetryDelay(5, TimeUnit.SECONDS)
        config.setAcquireRetryAttempts(3)
        config.setPoolName(poolName)
        Some(new BoneCP(config))
      } catch {
        case e: SQLException =>
          e.printStackTrace()
          None
      }
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }

}
