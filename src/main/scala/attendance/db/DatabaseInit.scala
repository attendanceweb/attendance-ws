package attendance.db

import attendance.config.Configuration
import org.flywaydb.core.Flyway
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

trait DatabaseInit {

  private val log: Logger = LoggerFactory.getLogger(getClass.getName)

  // TODO add error handling and retry
  def initDatabase(name: String, url: String, username: String, password: String): Option[ConnectionPool] = {
    val driver = Configuration.db.driver

    if (!driver.isEmpty && !url.isEmpty) {
      val pool = new ConnectionPool(driver, name, url, username, password)
      if (pool.initialized()) Some(pool) else None
    } else {
      log.error("Missing driver and/or url")
      None
    }
  }

  def migrate(url: String, username: String, password: String): Try[Unit] = {
    val flyway: Flyway = Flyway.configure().dataSource(url, username, password).load()
    Try(flyway.migrate())
  }
}
