package attendance.db

import org.slf4j.{Logger, LoggerFactory}

object DbUtil {
  val log: Logger = LoggerFactory.getLogger(getClass.getName)

  def herokuDbUrlToJdbcUrl(dbUrl: String): String = if (dbUrl.startsWith("jdbc:")) dbUrl else {
    val dbUrlRegexp = "postgres://(.*):(.*)@(.*):(\\d*)/(.*)".r
    val jdbcUrlOpt: Option[String] = dbUrlRegexp.findFirstMatchIn(dbUrl) match {
      case Some(m) =>
        val urlUsername = m.group(1)
        val urlPassword = m.group(2)
        val urlHost = m.group(3)
        val urlPort = m.group(4)
        val urlDbName = m.group(5)
        Some(s"jdbc:postgresql://$urlHost:$urlPort/$urlDbName?user=$urlUsername&password=$urlPassword")
      case None =>
        None
    }
    jdbcUrlOpt.getOrElse {
      log.warn("Cannot parse JDBC string: " + dbUrl)
      ""
    }
  }

}
