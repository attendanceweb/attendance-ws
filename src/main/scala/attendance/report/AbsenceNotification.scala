package attendance.report

import java.time.LocalDateTime

import attendance.absence.{AbsenceProvider, MemberAbsenceForSectionContact}
import attendance.config.Configuration
import attendance.db.ConnectionPool
import attendance.section.{SectionContact, SectionContactProvider}
import attendance.util.MailUtil
import attendance.util.datetime.DateUtil
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future

class AbsenceNotification(implicit db: ConnectionPool) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  val memberAbsenceProvider = new AbsenceProvider()
  val sectionContactProvider = new SectionContactProvider()

  def sendReports(fromDate: LocalDateTime, toDate: LocalDateTime): Seq[Future[Unit]] = {
    val absence = memberAbsenceProvider.absenceForSectionContactsBetween(Configuration.absenceLimit, fromDate, toDate)
    val absenceFiltered = absence.filter(_.membersAbsence.nonEmpty).filter(_.sectionContact.email.nonEmpty)
    val absenceDataAllStr = absenceFiltered.map(createBodyData).mkString("\n\n")
    val reportsToSections = absenceFiltered.map(a => sendReport(createBodyData(a), a.sectionContact.email.getOrElse(""), fromDate, toDate))
    val reportToChairman = sendReport(absenceDataAllStr, Configuration.notification.toChairman, fromDate, toDate)
    reportsToSections ++ Seq(reportToChairman)
  }

  private def sendReport(absenceDataStr: String, mailTo: String, fromDate: LocalDateTime, toDate: LocalDateTime): Future[Unit] = {
    val from = fromDate.format(DateUtil.ddMMyyyy)
    val to = toDate.format(DateUtil.ddMMyyyy)
    val title = s"AKF: frafall mellom $from - $to"
    val mailTo = Configuration.notification.toChairman
    val absenceLimit = Configuration.absenceLimit

    def bodyText(from: String, to: String, absenceDataStr: String): String = {
      val start = s"Hei!\n\nFølgende har vært borte fra mer enn $absenceLimit øvelser mellom $from - $to:"
      val end = "\n\nHilsen, Fremmøteappen\n" + Configuration.security.allowedOrigins
      start + "\n\n" + absenceDataStr + end
    }

    MailUtil.send(mailTo, title, Some(bodyText(from, to, absenceDataStr)), None)
  }

  private def createBodyData(absence: MemberAbsenceForSectionContact): String = {
    def createSectionContactTitle(sectionContact: SectionContact): String = {
      val sectionNames = sectionContact.sections.map(_.name).mkString(", ")
      s"Stemmegruppe(r): $sectionNames"
    }

    def createSectionContactData(absence: MemberAbsenceForSectionContact): String = {
      absence.membersAbsence.map { ma =>
        val memberProjectAbsence: String = ma.absence.map { mp =>
          val projectStr = mp.project.map(p => "- " + p.name + ": ").getOrElse("- ")
          val datesString = mp.absence.map(a => a.rehearsal_date_end.format(DateUtil.ddMM)).mkString(" ")
          projectStr + datesString
        }.mkString("\n")
        ma.first_name + " " + ma.last_name + (if (!ma.active) " (inaktiv)" else "") + ": \n" + memberProjectAbsence
      }.mkString("\n")
    }

    val title = createSectionContactTitle(absence.sectionContact)
    val body = createSectionContactData(absence)
    s"$title:\n$body"
  }
}
