package attendance.report

import java.time.LocalDateTime

import attendance.config.Configuration
import attendance.db.ConnectionPool
import attendance.util.MailUtil
import attendance.util.datetime.DateUtil
import org.apache.commons.lang3.exception.ExceptionUtils
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future

class AttendanceNotification(implicit database: ConnectionPool) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  val missingAttendanceProvider = new MissingAttendanceProvider()

  def sendNotification(): Seq[Future[Unit]] = {
    def bodyText(missing: MissingAttendanceForSectionContact): String = {
      val start = "Hei!\n\nHusk å registrere fremmøte for følgende:\n\n"
      val body: String = missing.forRehearsals.map { r =>
        val rehearsalTitle = "Øvelse " + r.rehearsal.dateStartFormatted
        val membersList = "Medlemmer: " + r.members.map(m => m.first_name + " " + m.last_name).mkString(", ")
        rehearsalTitle + "\n" + membersList
      }.mkString("\n\n")
      val end = "\n\nHilsen, Fremmøteappen\n" + Configuration.security.allowedOrigins
      start + body + end
    }

    missingAttendanceProvider.find(DateUtil.dateSemesterStart(), LocalDateTime.now()).map { missingList =>
      val missingWithScEmail = missingList.filter(m => m.sectionContact.email.nonEmpty).filter(m => m.forRehearsals.nonEmpty)
      missingWithScEmail.map { missing =>
        val to = missing.sectionContact.email.getOrElse("")
        val title = "AKF: fremmøteregistrering"
        logger.info("Sending notification to " + to)
        MailUtil.send(to, title, Some(bodyText(missing)), None)
      }
    }.recover {
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        Nil
    }
  }.get
}
