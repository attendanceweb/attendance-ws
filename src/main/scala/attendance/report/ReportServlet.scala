package attendance.report

import java.time.LocalDateTime

import attendance.api.{Response, RestApi}
import attendance.db.ConnectionPool
import attendance.util.FutureUtil
import attendance.util.datetime.DateUtil.{dateStrSemesterStart, localDateTime}
import attendance.util.datetime.{DateUtil, LocalDateSerializer, LocalDateTimeSerializer}
import org.apache.commons.lang3.exception.ExceptionUtils
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.{InternalServerError, Ok}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class ReportServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val absenceNotification = new AbsenceNotification()
  val attendanceNotification = new AttendanceNotification()
  val missingAttendanceProvider = new MissingAttendanceProvider()

  post("/absence") {
    val from = localDateTime(dateStrSemesterStart())
    val to = localDateTime
    logger.info("Sending absence report between " + from + " - " + to)
    val futures: Seq[Future[Unit]] = absenceNotification.sendReports(from, to)
    Await.ready(FutureUtil.waitAll(futures), Duration.Inf).map { result =>
      result.find(_.isFailure).map { failure =>
        logger.error("Failed to send reports")
        failure.recover {
          case t: Throwable => logger.error(ExceptionUtils.getStackTrace(t))
        }
        InternalServerError()
      }.getOrElse {
        logger.info("Sent reports")
        Ok(Response(0, "Sent reports"))
      }
    }
  }

  post("/absence/:from/:to") {
    val from = params.getOrElse("from", halt(400))
    val to = params.getOrElse("to", halt(400))
    logger.info("Sending absence report between " + from + " - " + to)
    val futures: Seq[Future[Unit]] = absenceNotification.sendReports(localDateTime(from), localDateTime(to))
    Await.ready(FutureUtil.waitAll(futures), Duration.Inf).map { result =>
      result.find(_.isFailure).map { failure =>
        logger.error("Failed to send reports")
        failure.recover {
          case t: Throwable => logger.error(ExceptionUtils.getStackTrace(t))
        }
        InternalServerError()
      }.getOrElse {
        logger.info("Sent reports")
        Ok(Response(0, "Sent reports"))
      }
    }
  }

  get("/attendance") {
    logger.info("Get missing attendance registration")
    missingAttendanceProvider.find(DateUtil.dateSemesterStart(), LocalDateTime.now()).get
  }

  post("/attendance") {
    logger.info("Notify of missing attendance registration")
    val futures: Seq[Future[Unit]] = attendanceNotification.sendNotification()
    Await.ready(FutureUtil.waitAll(futures), Duration.Inf).map { result =>
      result.find(_.isFailure).map { failure =>
        logger.error("Failed to send notifications")
        failure.recover {
          case t: Throwable => logger.error(ExceptionUtils.getStackTrace(t))
        }
        InternalServerError()
      }.getOrElse {
        logger.info("Sent notifications")
        Ok(Response(0, "Sent notifications"))
      }
    }
  }
}
