package attendance.report

import attendance.member.Member
import attendance.rehearsal.Rehearsal
import attendance.section.SectionContact

case class MissingAttendanceForSectionContact(sectionContact: SectionContact, forRehearsals: List[MissingAttendanceForRehearsal])

case class MissingAttendanceForRehearsal(rehearsal: Rehearsal, members: List[Member])