package attendance.report

import java.time.LocalDateTime

import anorm._
import attendance.db.ConnectionPool
import attendance.member.{Member, MemberProvider}
import attendance.rehearsal.{Rehearsal, RehearsalProvider}
import attendance.section.{SectionContact, SectionContactProvider}
import attendance.util.TryWith
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

class MissingAttendanceProvider(implicit db: ConnectionPool) {

  val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  val parser: RowParser[MissingAttendance] = Macro.namedParser[MissingAttendance]

  val sectionContactProvider = new SectionContactProvider()
  val rehearsalProvider = new RehearsalProvider()
  val memberProvider = new MemberProvider()

  def find(startDate: LocalDateTime, endDate: LocalDateTime): Try[List[MissingAttendanceForSectionContact]] = {
    val rehearsals = rehearsalProvider.all()
    val sectionContacts = sectionContactProvider.all()

    val invalidRehersal = Rehearsal(-1, LocalDateTime.now(), LocalDateTime.now(), obligatory = true, None)

    findMissingAttendance(startDate, endDate).map { missing =>
      def missingPerRehearsal: List[MissingAttendanceForRehearsal] = {
        missing.groupBy(m => rehearsals.find(_.id == m.rehearsal_id).getOrElse(invalidRehersal)).map { entry =>
          val members: List[Member] = memberProvider.findActive(entry._1.date_start)
          val missingMembers = members.filter(m => entry._2.exists(mm => mm.member_id == m.member_id))
          (entry._1, missingMembers)
        }.map(entry => MissingAttendanceForRehearsal(entry._1, entry._2)).toList
      }

      def filterMissingPerRehearsal(sectionContact: SectionContact): List[MissingAttendanceForRehearsal] = {
        missingPerRehearsal.flatMap { entry =>
          val rehearsal = entry.rehearsal
          val members: List[Member] = entry.members.filter(m => sectionContact.sections.map(_.id).contains(m.section_id))
          if (members.isEmpty) {
            None
          } else {
            Some(MissingAttendanceForRehearsal(rehearsal, members))
          }
        }
      }

      sectionContacts.map(sc => MissingAttendanceForSectionContact(sc, filterMissingPerRehearsal(sc)))
    }
  }

  private def findMissingAttendance(startDate: LocalDateTime, endDate: LocalDateTime): Try[List[MissingAttendance]] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
        SELECT rehearsal.id AS rehearsal_id, member_active.member_id AS member_id
        FROM rehearsal
        JOIN member_active ON member_active.date_from<=rehearsal.date_start
         AND (member_active.date_to>rehearsal.date_end OR member_active.date_to IS NULL)
         AND rehearsal.obligatory=1
         AND rehearsal.date_start>=$startDate
         AND rehearsal.date_end<$endDate
        LEFT JOIN member_attendance ON member_active.member_id=member_attendance.member_id
         AND rehearsal.id=member_attendance.rehearsal_id WHERE member_attendance.member_id IS NULL
        ORDER BY rehearsal.id
      """.as(parser.*)
    }
  }
}
