package attendance.report

case class MissingAttendance(rehearsal_id: Long, member_id: Long)
