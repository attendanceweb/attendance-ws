package attendance.section

import anorm._
import attendance.db.ConnectionPool
import attendance.util.TryWith

class SectionContactProvider(implicit db: ConnectionPool) {

  private case class SectionContactDb(
                             id: Int,
                             section_id: Int,
                             member_id: Int,
                             first_name: String,
                             last_name: String,
                             email: Option[String],
                             section_name: String
                           )

  private val parser: RowParser[SectionContactDb] = Macro.namedParser[SectionContactDb]

  def byMemberId(member_id:Int): Option[SectionContact] = {
    val scDbList = TryWith(db.connection.get) { implicit con =>
      SQL"""
      SELECT
        section_contact.id, section_contact.section_id, section_contact.member_id,
        member.first_name, member.last_name, section_contact.email,
        section.name AS section_name
      FROM section_contact
      JOIN member ON member.member_id = section_contact.member_id
      JOIN section ON section.id = section_contact.section_id
      WHERE section_contact.member_id = $member_id
       AND member.date_changed IS NULL
      ORDER BY section_contact.section_id
    """.as(parser.*)
    }.get
    toSectionContact(scDbList).headOption
  }

  private def toSectionContact(scDbList: List[SectionContactDb]): List[SectionContact] = {
    val scDbByScId: Map[Int, List[SectionContactDb]] = scDbList.groupBy(scDb => scDb.member_id)
    scDbByScId.keySet.map(member_id => {
      val scTmpDbList: List[SectionContactDb] = scDbByScId(member_id)
      val sectionsForSc = scTmpDbList.map(sc => Section(sc.section_id, sc.section_name))
      SectionContact(member_id, scTmpDbList.head.section_id, scTmpDbList.head.first_name, scTmpDbList.head.last_name, scTmpDbList.head.email, sectionsForSc)
    }).toList
  }

  def all(): List[SectionContact] = {
    val scDbList = TryWith(db.connection.get) { implicit con =>
      SQL"""
        SELECT
          section_contact.id, section_contact.section_id, section_contact.member_id,
          member.first_name, member.last_name, section_contact.email,
          section.name AS section_name
        FROM section_contact
        JOIN member ON member.member_id = section_contact.member_id
        JOIN section ON section.id = section_contact.section_id
        WHERE member.date_changed IS NULL
        ORDER BY section_contact.section_id
      """.as(parser.*)
    }.get
    toSectionContact(scDbList)
  }

  def add(sectionId: Long, memberId: Long, email: String): Option[Long] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
        INSERT INTO section_contact (section_id, member_id, email)
        VALUES ($sectionId, $memberId, $email)
      """.executeInsert()
    }.get
  }

  def removeForMember(sectionContact: SectionContact): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          DELETE FROM section_contact
          WHERE member_id = ${sectionContact.member_id}
        """.executeUpdate()
    }.get
  }

  def removeAll(): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM section_contact""".executeUpdate()
    }.get
  }

}
