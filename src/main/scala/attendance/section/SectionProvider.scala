package attendance.section

import anorm._
import attendance.db.ConnectionPool
import attendance.util.TryWith

class SectionProvider(implicit db: ConnectionPool) {

  private val parser: RowParser[Section] = Macro.namedParser[Section]

  def byId(id: Int): Option[Section] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM section WHERE id = $id""".as(parser.*)
    }.get.headOption
  }

  def forSectionContact(member_id: Int): List[Section] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT *
          FROM section
          WHERE section.id IN (SELECT section_id FROM section_contact WHERE member_id = $member_id)
        """.as(parser.*)
    }.get
  }

  def all(): List[Section] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM section""".as(parser.*)
    }.get
  }

  def add(name: String): Option[Long] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""INSERT INTO section (name) VALUES ($name)""".executeInsert()
    }.get
  }

  def remove(section: Section): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM section WHERE id = ${section.id}""".executeUpdate()
    }.get
  }

  def removeAll(): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM section""".executeUpdate()
    }.get
  }
}
