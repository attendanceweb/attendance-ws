package attendance.section

case class SectionContact(
                           member_id: Int,
                           section_id: Int,
                           first_name: String,
                           last_name: String,
                           email: Option[String],
                           sections: List[Section]
                         )
