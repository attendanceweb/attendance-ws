package attendance.section

import attendance.api.RestApi
import attendance.db.ConnectionPool
import attendance.util.datetime.{LocalDateSerializer, LocalDateTimeSerializer}
import org.json4s.{DefaultFormats, Formats}

class SectionContactsServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val sectionContactProvider = new SectionContactProvider()

  get("/:member_id") {
    val member_id = params.getOrElse("member_id", halt(400)).toInt
    logger.info("Get section contact for member id " + member_id)
    sectionContactProvider.byMemberId(member_id)
  }

  get("/") {
    logger.info("Get section contacts")
    sectionContactProvider.all()
  }
}
