package attendance.section

import attendance.api.RestApi
import attendance.db.ConnectionPool
import attendance.util.datetime.{LocalDateSerializer, LocalDateTimeSerializer}
import org.json4s.{DefaultFormats, Formats}

class SectionsServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val sectionProvider = new SectionProvider()

  get("/:id") {
    val id = params.getOrElse("id", halt(400)).toInt
    logger.info("Get section id " + id)
    sectionProvider.byId(id)
  }

  get("/") {
    logger.info("Get sections")
    sectionProvider.all()
  }
}
