package attendance.rehearsal

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

case class Rehearsal(
  id: Int,
  date_start: LocalDateTime,
  date_end: LocalDateTime,
  obligatory: Boolean,
  information: Option[String]) {

  def dateStartFormatted: String = {
    date_start.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"))
  }
}
