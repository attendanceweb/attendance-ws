package attendance.rehearsal

import attendance.api.{Response, RestApi}
import attendance.db.ConnectionPool
import attendance.util.datetime.{LocalDateSerializer, LocalDateTimeSerializer}
import org.json4s.{DefaultFormats, Formats, _}
import org.scalatra._

import scala.util.Try

class RehearsalsServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats + LocalDateSerializer + LocalDateTimeSerializer

  implicit val database: ConnectionPool = db
  val rehearsalProvider = new RehearsalProvider()

  get("/:id") {
    val id = params.getOrElse("id", halt(400)).toLong
    logger.info("Get rehearsal id " + id)
    rehearsalProvider.byId(id)
  }

  get("/") {
    logger.info("Get rehearsals")
    rehearsalProvider.all()
  }

  post("/") {
    Try {
      val json = parse(request.body)
      json.extract[Rehearsal]
    }.map { rehearsal =>
      logger.info("Add rehearsal")
      val id = rehearsalProvider.add(rehearsal)
      if (id.isEmpty) {
        logger.error("Failed adding rehearsal " + rehearsal)
        InternalServerError("Failed adding rehearsal")
      } else {
        val intId = id.get.toInt
        logger.info("Added rehearsal id=" + intId)
        Ok(rehearsal.copy(id = intId))
      }
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  put("/:id") {
    val id = params.getOrElse("id", halt(400)).toLong
    logger.info("Modify rehearsal id " + id)

    Try {
      val json = parse(request.body)
      json.extract[Rehearsal]
    }.map { rehearsal =>
      logger.info("Modifying rehearsal id " + id + " with " + rehearsal)
      val count = rehearsalProvider.modify(rehearsal)
      if (count >= 1) {
        logger.info("Modified")
        Ok(Response(0, "Modified rehearsal"))
      } else {
        logger.error("Modifying failed: rehearsal not found")
        BadRequest("Modifying failed: rehearsal not found")
      }
    }.recover {
      case _: Throwable =>
        logger.error("Bad Request")
        BadRequest()
    }.get
  }

  delete("/:id") {
    val id = params.getOrElse("id", halt(400)).toLong
    logger.info("Remove rehearsal id " + id)
    val count = rehearsalProvider.remove(id)
    if (count >= 1) {
      logger.info("Removed")
      Ok(Response(0, "Removed rehearsal"))
    } else {
      logger.error("Removal failed: rehearsal not found")
      BadRequest("Removal failed: rehearsal not found")
    }
  }
}
