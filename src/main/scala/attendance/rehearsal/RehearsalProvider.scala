package attendance.rehearsal

import java.time.LocalDateTime

import anorm.SqlParser.get
import anorm._
import attendance.db.ConnectionPool
import attendance.util.TryWith
import attendance.util.datetime.DateUtil._

class RehearsalProvider(implicit db: ConnectionPool) {

  val parser: RowParser[Rehearsal] =
    get[Int]("id") ~
      get[LocalDateTime]("date_start") ~
      get[LocalDateTime]("date_end") ~
      get[Int]("obligatory") ~
      get[Option[String]]("information") map {
      case id ~ date_start ~ date_end ~ obligatory ~ information =>
        Rehearsal(id, date_start, date_end, obligatory > 0, information)
    }

  def closestTo(dateTime: LocalDateTime): Option[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT *
          FROM rehearsal
          WHERE date_start=
            (SELECT date_start
              FROM rehearsal
              ORDER BY ABS(EXTRACT(epoch FROM (date_start - $dateTime)))
              LIMIT 1
            )
        """.as(parser.*)
    }.get.headOption
  }

  def byId(id: Long): Option[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM rehearsal WHERE id = $id""".as(parser.*)
    }.get.headOption
  }

  def previous(rehearsal: Rehearsal): Option[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            *
          FROM rehearsal
    		  WHERE date_start < ${rehearsal.date_start}
    		    AND date_end < ${rehearsal.date_end}
          ORDER BY date_start DESC, date_end DESC
        """.as(parser.*)
    }.get.headOption
  }

  def next(rehearsal: Rehearsal): Option[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            *
          FROM rehearsal
    		  WHERE date_start >= ${rehearsal.date_start}
    		    AND date_end > ${rehearsal.date_end}
          ORDER BY date_start ASC, date_end ASC
        """.as(parser.*)
    }.get.headOption
  }

  def between(startDate: LocalDateTime, endDate: LocalDateTime): List[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
        SELECT *
        FROM rehearsal
        WHERE date_start >= $startDate AND date_end < $endDate"""
      .as(parser.*)
    }.get
  }

  def all(): List[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""SELECT * FROM rehearsal""".as(parser.*)
    }.get
  }

  def add(rehearsal: Rehearsal): Option[Long] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          INSERT INTO rehearsal (date_start, date_end, obligatory, information)
          VALUES (${rehearsal.date_start}, ${rehearsal.date_end}, ${if (rehearsal.obligatory) 1 else 0}, ${rehearsal.information})
        """.executeInsert()
    }.get
  }

  def modify(rehearsal: Rehearsal): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          UPDATE rehearsal SET
            date_start=${rehearsal.date_start},
            date_end=${rehearsal.date_end},
            obligatory=${if (rehearsal.obligatory) 1 else 0},
            information=${rehearsal.information}
          WHERE id=${rehearsal.id}
        """.executeUpdate()
    }.get
  }

  def remove(rehearsalId: Long): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM rehearsal WHERE id = $rehearsalId""".executeUpdate()
    }.get
  }

  def removeAll(): Int = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""DELETE FROM rehearsal""".executeUpdate()
    }.get
  }

  def forAbsentMember(member_id: Int): List[Rehearsal] = {
    TryWith(db.connection.get) { implicit con =>
      SQL"""
          SELECT
            *
          FROM rehearsal
          JOIN member_attendance ON rehearsal.id = member_attendance.rehearsal_id
          WHERE member_attendance.member_id = $member_id
            AND rehearsal.date_start >= ${dateSemesterStart()}
            AND rehearsal.present <> 1
        """.as(parser.*)
    }.get
  }

  def firstInSemester(): Option[Rehearsal] = {
    closestTo(dateSemesterStart())
  }
}
