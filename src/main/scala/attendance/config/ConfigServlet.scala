package attendance.config

import attendance.api.RestApi
import attendance.db.ConnectionPool
import org.apache.commons.lang3.exception.ExceptionUtils
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.InternalServerError

class ConfigServlet(db: ConnectionPool) extends RestApi {
  protected implicit val jsonFormats: Formats = DefaultFormats

  implicit val database: ConnectionPool = db
  val configFromDbProvider = new ConfigFromDbProvider()

  get("/") {
    logger.info("Get configuration")
    configFromDbProvider.all().map { result =>
      result
    }.recover {
      case t: Throwable =>
        logger.error(ExceptionUtils.getStackTrace(t))
        InternalServerError()
    }.get
  }
}
