package attendance.config

class AppConfig(props: Map[String, String]) {

  def asString(key: String) = {
    props.get(key)
  }

  def asInt(key: String) = {
    props.get(key) match {
      case Some(x) => Some(x.toInt)
      case _ => None
    }
  }

  def all: Map[String, String] = props
}

object AppConfig {

  def apply(props: Map[String, String]) = {
    new AppConfig(props)
  }

}
