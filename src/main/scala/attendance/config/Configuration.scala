package attendance.config

import java.nio.file.{Files, Paths}

import attendance.db.{ConnectionPool, DbUtil}
import org.slf4j.{Logger, LoggerFactory}

class Configuration(implicit db: ConnectionPool) {
  private val configFromDbProvider: ConfigFromDbProvider = new ConfigFromDbProvider()

  private def get(name: String) = configFromDbProvider.getSingle(name).map(_.getOrElse("")).get
  object providers {

    object sheets {
      def apiKey: String = get("providers.sheets.apikey")

      def public: Boolean = get("providers.sheets.public").toBoolean

      def clientId: String = get("providers.sheets.clientid")

      def spreadsheetId: String = get("providers.sheets.sheetid")

      def spreadsheetRange: String = get("providers.sheets.sheetrange")
    }

  }
}

object Configuration {

  private val log: Logger = LoggerFactory.getLogger(getClass.getName)

  def loadDevConfIfExists(): Unit = {
    val confFilePath = "conf/dev-config.conf"
    val confOpt: Option[AppConfig] = loadConf(confFilePath)
    confOpt.foreach { conf =>
      log.warn(s"Found development configuration file $confFilePath")
      conf.all.foreach { prop =>
        log.warn("Overriding property: " + prop._1)
        System.setProperty(prop._1, prop._2)
      }
    }
  }

  private def loadConf(confFilePath: String): Option[AppConfig] = {
    val fileExists = Files.exists(Paths.get(confFilePath))

    fileExists match {
      case true => Some(new ConfigReader().fromPropsFile(confFilePath))
      case false => None
    }
  }

  def port: Int = Option(System.getProperty("http.port")).getOrElse("8080").toInt

  def isProduction: Boolean = !isDevelopment
  def isDevelopment: Boolean = env == "development"
  def env: String = Option(System.getProperty("env")).getOrElse("production")

  def absenceLimit: Int = 1

  object db {
    def driver = "org.postgresql.Driver"

    def url: String = Option(System.getProperty("db.url")).map(DbUtil.herokuDbUrlToJdbcUrl).getOrElse("postgresql://localhost/attendance")

    def username: String = Option(System.getProperty("db.username")).getOrElse("")

    def password: String = Option(System.getProperty("db.password")).getOrElse("")
  }

  object notification {
    def host: String = Option(System.getProperty("notification.host")).getOrElse("localhost")

    def port: Int = Option(System.getProperty("notification.port")).getOrElse("587").toInt

    def user: String = Option(System.getProperty("notification.user")).getOrElse("")

    def password: String = Option(System.getProperty("notification.password")).getOrElse("")

    def from: String = Option(System.getProperty("notification.from")).getOrElse("")

    def toChairman: String = Option(System.getProperty("notification.toChairman")).getOrElse("")
  }

  object security {
    def allowedOrigins: String = Option(System.getProperty("security.allowedOrigins")).getOrElse("http://localhost:9000")
  }
}