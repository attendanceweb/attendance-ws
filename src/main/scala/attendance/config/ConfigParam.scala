package attendance.config

case class ConfigParam(name: String, value: Option[String])