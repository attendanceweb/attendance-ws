package attendance.config
import anorm.{Macro, RowParser, SQL, SqlParser}
import attendance.db.ConnectionPool
import attendance.util.TryWith
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try

class ConfigFromDbProvider(implicit db: ConnectionPool) {

  private val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  private val parser: RowParser[ConfigParam] = Macro.namedParser[ConfigParam]

  def getSingle(name: String): Try[Option[String]] = {
    get(name).map(_.headOption)
  }

  def get(name: String): Try[List[String]] = {
    val sql = "SELECT value FROM config WHERE name={name}"
    val parser = SqlParser.scalar[String]
    TryWith(db.connection.get) { implicit con => SQL(sql).as(parser.*) }
  }

  def all(): Try[List[ConfigParam]] = {
    val sql = "SELECT name, value FROM config"
    TryWith(db.connection.get) { implicit con => SQL(sql).as(parser.*) }
  }
}
