package attendance.config

import scala.io.Source

class ConfigReader {

  def fromPropsFile(path: String): AppConfig = {
    val map = Source.fromFile(path)
      .getLines()
      .filterNot(line => line.trim.isEmpty)
      .filterNot(line => line.startsWith("#"))
      .map(splitLine)
      .toMap

    AppConfig(map)
  }

  private def splitLine(line: String) = {
    val idx = line.indexOf("=")
    (line.substring(0, idx).trim, line.substring(idx + 1).trim)
  }

}
