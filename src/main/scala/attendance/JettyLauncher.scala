package attendance

import org.eclipse.jetty.server._
import org.eclipse.jetty.server.handler.{HandlerCollection, RequestLogHandler}
import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.webapp.WebAppContext
import org.scalatra.servlet.ScalatraListener
import org.slf4j.LoggerFactory

object JettyLauncher {
  private val log = LoggerFactory.getLogger(getClass.getName)

  def main(args: Array[String]) {
    val port = Option(System.getProperty("http.port")).getOrElse("8080").toInt
    val contextPath = Option(System.getProperty("http.context_path")).getOrElse("/")

    val server = new Server
    server.setStopTimeout(5000)
    server.setStopAtShutdown(true)

    val httpConf = new HttpConfiguration()
    httpConf.setSendServerVersion(false)
    httpConf.setSendDateHeader(true)
    httpConf.setSendXPoweredBy(false)

    val connector = new ServerConnector(server, new HttpConnectionFactory(httpConf))
    connector.setHost("0.0.0.0")
    connector.setPort(port)
    connector.setIdleTimeout(30000)
    server.addConnector(connector)

    val webAppContext = new WebAppContext
    val webDir = getClass.getClassLoader.getResource("WEB-INF").toExternalForm
    webAppContext.setResourceBase(webDir)
    webAppContext.setContextPath(contextPath)
    webAppContext.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false")
    webAppContext.setEventListeners(Array(new ScalatraListener))
    webAppContext.addServlet(classOf[DefaultServlet], "/")

    val requestLog = new Slf4jRequestLog()
    requestLog.setExtended(false)
    val requestLogHandler = new RequestLogHandler()
    requestLogHandler.setRequestLog(requestLog)

    val handlers = new HandlerCollection
    handlers.addHandler(webAppContext)
    handlers.addHandler(requestLogHandler)

    server.setHandler(handlers)

    server.start
    server.join
  }

}
