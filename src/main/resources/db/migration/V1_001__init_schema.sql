CREATE TABLE config (
    id integer NOT NULL,
    name text NOT NULL,
    value text
);

CREATE SEQUENCE config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE config_id_seq OWNED BY config.id;

CREATE TABLE member (
    id integer NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    section_id integer,
    date_changed timestamp without time zone,
    member_id integer NOT NULL
);

CREATE TABLE member_active (
    id integer NOT NULL,
    member_id integer NOT NULL,
    date_from timestamp without time zone NOT NULL,
    date_to timestamp without time zone
);

CREATE SEQUENCE member_active_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE member_active_id_seq OWNED BY member_active.id;

CREATE TABLE member_attendance (
    id integer NOT NULL,
    rehearsal_id integer,
    member_id integer NOT NULL,
    present integer DEFAULT 1 NOT NULL
);

CREATE SEQUENCE member_attendance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE member_attendance_id_seq OWNED BY member_attendance.id;

CREATE SEQUENCE member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE member_id_seq OWNED BY member.id;

CREATE TABLE project (
    id integer NOT NULL,
    name text NOT NULL
);

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE project_id_seq OWNED BY project.id;

CREATE TABLE project_start_end (
    id integer NOT NULL,
    project_id integer NOT NULL,
    date_start timestamp without time zone NOT NULL,
    date_end timestamp without time zone NOT NULL
);

CREATE SEQUENCE project_start_end_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE project_start_end_id_seq OWNED BY project_start_end.id;

CREATE TABLE rehearsal (
    id integer NOT NULL,
    date_start timestamp without time zone NOT NULL,
    date_end timestamp without time zone NOT NULL,
    obligatory integer DEFAULT 1,
    information text
);

CREATE SEQUENCE rehearsal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE rehearsal_id_seq OWNED BY rehearsal.id;

CREATE TABLE section (
    id integer NOT NULL,
    name text NOT NULL
);

CREATE TABLE section_contact (
    id integer NOT NULL,
    section_id integer,
    member_id integer NOT NULL,
    email text
);

CREATE SEQUENCE section_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE section_contact_id_seq OWNED BY section_contact.id;

CREATE SEQUENCE section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE section_id_seq OWNED BY section.id;

ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);

ALTER TABLE ONLY member ALTER COLUMN id SET DEFAULT nextval('member_id_seq'::regclass);

ALTER TABLE ONLY member_active ALTER COLUMN id SET DEFAULT nextval('member_active_id_seq'::regclass);

ALTER TABLE ONLY member_attendance ALTER COLUMN id SET DEFAULT nextval('member_attendance_id_seq'::regclass);

ALTER TABLE ONLY project ALTER COLUMN id SET DEFAULT nextval('project_id_seq'::regclass);

ALTER TABLE ONLY project_start_end ALTER COLUMN id SET DEFAULT nextval('project_start_end_id_seq'::regclass);

ALTER TABLE ONLY rehearsal ALTER COLUMN id SET DEFAULT nextval('rehearsal_id_seq'::regclass);

ALTER TABLE ONLY section ALTER COLUMN id SET DEFAULT nextval('section_id_seq'::regclass);

ALTER TABLE ONLY section_contact ALTER COLUMN id SET DEFAULT nextval('section_contact_id_seq'::regclass);

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);

ALTER TABLE ONLY member_active
    ADD CONSTRAINT member_active_pkey PRIMARY KEY (id);

ALTER TABLE ONLY member_attendance
    ADD CONSTRAINT member_attendance_pkey PRIMARY KEY (id);

ALTER TABLE ONLY member
    ADD CONSTRAINT member_pkey PRIMARY KEY (id);

ALTER TABLE ONLY project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);

ALTER TABLE ONLY project_start_end
    ADD CONSTRAINT project_start_end_pkey PRIMARY KEY (id);

ALTER TABLE ONLY rehearsal
    ADD CONSTRAINT rehearsal_pkey PRIMARY KEY (id);

ALTER TABLE ONLY section_contact
    ADD CONSTRAINT section_contact_pkey PRIMARY KEY (id);

ALTER TABLE ONLY section
    ADD CONSTRAINT section_pkey PRIMARY KEY (id);

ALTER TABLE ONLY member_attendance
    ADD CONSTRAINT member_attendance_rehearsal_id_fkey FOREIGN KEY (rehearsal_id) REFERENCES rehearsal(id);

ALTER TABLE ONLY member
    ADD CONSTRAINT member_section_id_fkey FOREIGN KEY (section_id) REFERENCES section(id);

ALTER TABLE ONLY section_contact
    ADD CONSTRAINT section_contact_section_id_fkey FOREIGN KEY (section_id) REFERENCES section(id);
