#!/bin/bash

dir="$(dirname $0)"

if [[ ! -z $(grep "Invalid value token at ' + formatPosition" "$dir/../node_modules/clean-css/lib/optimizer/level-1/optimize.js") ]]; then
    patch -p1 < "$dir/clean-css.patch"
fi
