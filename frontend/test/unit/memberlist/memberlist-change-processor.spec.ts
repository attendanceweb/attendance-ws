import {MemberlistChangeProcessor} from "../../../src/memberlist/memberlist-change-processor";
import {Member} from "../../../src/model/member";

describe('Member list change processor', () => {
  function createMember(memberId: number, firstName: string, lastName: string, sectionId: number, dateFrom: string, dateTo: string, dateChanged: string) {
    let member = new Member();
    member.member_id = memberId;
    member.first_name = firstName;
    member.last_name = lastName;
    member.section_id = sectionId;
    member.date_from = dateFrom;
    member.date_to = dateTo;
    member.date_changed = dateChanged;
    return member;
  }

  it('should find all members that are missing in destination', () => {
    const source: Member[] = [
      createMember(3, "X", "Y", 1, "2018-01-01T10:00", undefined, undefined),
      createMember(4, "Test", "Testesen", 1, "2018-01-01T10:00", undefined, undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
    ];
    const destination: Member[] = [
      createMember(1, "A", "B", 3, "2018-01-01T10:00", undefined, undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
    ];

    const members = MemberlistChangeProcessor.findMembersNotPresentInDestination(source, destination);

    expect(members).toMatchSnapshot();
  });

  it('should find all members that are present but not active in destination', () => {
    const source: Member[] = [
      createMember(1, "A", "B", 3, "2018-03-01T10:00", undefined, undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
    ];
    const destination: Member[] = [
      createMember(1, "A", "B", 3, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
    ];

    const members = MemberlistChangeProcessor.findMembersPresentButNotActiveInDestination(source, destination);

    expect(members).toMatchSnapshot();
  });

  it('should find all active members that are not present in source', () => {
    const source: Member[] = [
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
    ];
    const destination: Member[] = [
      createMember(1, "A", "B", 3, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
      createMember(3, "C", "D", 2, "2018-01-01T10:00", undefined, undefined),
    ];

    const members = MemberlistChangeProcessor.findMembersNotPresentInSource(source, destination);

    expect(members).toMatchSnapshot();
  });

  it('should find all modified active members', () => {
    const source: Member[] = [
      createMember(1, "A", "B", 3, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
      createMember(3, "C", "D", 2, "2018-01-01T10:00", undefined, undefined),
      createMember(4, "E", "F", 1, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(5, "G", "H", 5, "2018-01-01T10:00", undefined, undefined),
      createMember(6, "I", "J", 5, "2018-01-01T10:00", undefined, undefined),
    ];
    const destination: Member[] = [
      createMember(1, "A", "B", 3, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(2, "Test", "Tullesen", 4, "2018-01-01T10:00", undefined, undefined),
      createMember(3, "C", "XX", 2, "2018-01-01T10:00", undefined, undefined),
      createMember(4, "E", "F", 2, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(5, "G", "H", 2, "2018-01-01T10:00", "2018-02-01T10:00", undefined),
      createMember(6, "I", "J", 6, "2018-01-01T10:00", undefined, undefined),
    ];

    const members = MemberlistChangeProcessor.findModifiedMembers(source, destination);

    expect(members).toMatchSnapshot();
  });
});
