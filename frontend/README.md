# Attendance Registration web interface ##

## Start in development ##
Preparation:
   npm install
   npm install webpack -g
   npm install nps -g

Install Heroku CLI

Create .env file based on .env.example

Start:
   heroku local web

## Integrations

### Attendance web service
This is the web service that manages attendance. In order for this integration to work, the URL of the web service has to be set in the `BACKEND_URL` environment variable.

### Google Spreadsheets
The page 'Members' has an integration with Google Spreadsheets in order to retrieve the current members list and allow importing them into the Attendance application.

The integration requires the following environment variables to be set:
- `GOOGLE_API_KEY`. It can be generated via the Google API dashboard (https://console.developers.google.com).
- `GOOGLE_SPREADSHEET_ID`. Example: if your Google spreadsheet with members list is available at https://docs.google.com/spreadsheets/d/XXX, then `GOOGLE_SPREADSHEET_ID` has to be set to "XXX".
- `GOOGLE_SPREADSHEET_RANGE`. Example: "A2:Z1000".
