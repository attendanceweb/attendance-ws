import {Message, Severity} from "./message";

export class PageWithMessage {

  message: Message;

  constructor() {
    this.resetMessage();
  }

  resetMessage() {
    this.message = new Message(null, null);
  }

  setMessage(text: string, severity: Severity) {
    this.message = new Message(text, severity);
  }
  
  isError(): boolean {
    return this.message.severity === Severity.ERROR;
  }
}
