export enum Severity {
  ERROR,
  WARNING,
  INFO
}
export class Message {
  constructor(public text: string, public severity: Severity) {
  }
}
