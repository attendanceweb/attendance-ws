import {Aurelia, inject} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router';
import {PLATFORM} from 'aurelia-pal';
import {I18N} from "aurelia-i18n";

@inject(I18N)
export class App {
  router: Router;

  constructor(public i18n: I18N) {
    this.i18n = i18n;
  }

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Attendance';
    config.map([
      {route: ['', 'register'], name: 'register', moduleId: PLATFORM.moduleName('./register'), nav: true, title: this.i18n.tr('nav:register')},
      {route: 'reports', name: 'reports', moduleId: PLATFORM.moduleName('reports'), nav: true, title: this.i18n.tr('nav:reports')},
      {route: 'memberlist', name: 'memberlist', moduleId: PLATFORM.moduleName('memberlist'), nav: true, title: this.i18n.tr('nav:memberlist')},
      {route: 'admin', name: 'admin', moduleId: PLATFORM.moduleName('admin'), nav: true, title: this.i18n.tr('nav:admin')},
    ]);

    this.router = router;
  }
}
