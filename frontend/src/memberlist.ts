/// <reference path="../node_modules/@types/gapi/index.d.ts" />

import {inject} from 'aurelia-framework';
import {GoogleSheetsService} from "./memberlist/google-sheets-service";
import {Member} from "./model/member";
import {BackendService} from "./backend-service";
import {Section} from "./model/section";
import {MemberImportAction, MemberImportActionType} from "./memberlist/member-import-action";
import {Util} from "./util";
import {Severity} from "./message";
import {PageWithMessage} from "./page-with-message";
import {MemberWithActions, MemberActionsForSection} from "./memberlist/member-with-actions";
import {MemberlistChangeProcessor} from "./memberlist/memberlist-change-processor";

declare var gapi: any;

@inject(GoogleSheetsService, BackendService)
export class MemberList extends PageWithMessage {

  MemberImportActionType = MemberImportActionType;

  initializing: boolean = true;
  
  sections: Section[] = [];
  memberActionsForSections: MemberActionsForSection[] = [];

  importActionsCount = 0;

  memberIdColumnIndex = 0;
  firstNameColumnIndex = 1;
  lastNameColumnIndex = 2;
  sectionIdColumnIndex = 9;
  activeColumnIndex = 11;
  dateFromColumnIndex = 12;

  constructor(private googleSheetsService: GoogleSheetsService, private backendService: BackendService) {
    super();
    this.googleSheetsService.init(result => {
      let sectionsPromise = this.backendService.getSections();
      let membersInDbPromise = this.backendService.getMembers();
      let membersInGooglePromise = this.googleSheetsService.getValues();
      Promise.all([sectionsPromise, membersInDbPromise, membersInGooglePromise]).then(result => {
        this.sections = result[0];
        const membersInDb = result[1];
        const membersInGoogle = this.initMembersInGoogle(result[2]);
        this.memberActionsForSections = MemberlistChangeProcessor.initMembersWithActions(membersInGoogle, membersInDb);
        this.memberActionsForSections.forEach(m => m.membersWithActions.forEach(ma => this.importActionsCount += ma.importActions.length));
        this.initializing = false;
      }, error => {
        this.setMessage("Failed to initialize data: " + error, Severity.ERROR);
        this.initializing = false;
      });
    }, err => {
      this.setMessage("Failed to load the Google Spreadsheet API", Severity.ERROR);
      this.initializing = false;
    }, err => {
      this.setMessage("Failed to initialize the Google Spreadsheet API", Severity.ERROR);
      this.initializing = false;
    })
  }

  initMembersInGoogle(response: any): Member[] {
    return response.result.values
      .filter(row => row[this.activeColumnIndex] === "" || row[this.activeColumnIndex] === null || row[this.activeColumnIndex] === undefined)
      .map(row => {
        let member = new Member();
        member.member_id = Number(row[this.memberIdColumnIndex]);
        member.first_name = row[this.firstNameColumnIndex];
        member.last_name = row[this.lastNameColumnIndex];
        member.section_id = this.sectionNameToId(row[this.sectionIdColumnIndex]);
        member.date_from = row[this.dateFromColumnIndex] + "T00:00";
        return member;
      });
  }

  private sectionNameToId(sectionName: string): number {
    let section = this.sections.find(s => s.name === sectionName);
    if (section !== undefined) {
      return section.id;
    } else {
      return undefined;
    }
  }

  sectionIdToName(sectionId: number): string {
    let section = this.sections.find(s => s.id === sectionId);
    if (section !== undefined) {
      return section.name;
    } else {
      return undefined;
    }
  }

  runAction(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    importAction.clicked = true;
    if (importAction.actionType === MemberImportActionType.ACTIVATE) {
      this.activateMember(memberWithActions, importAction);
    } else if (importAction.actionType === MemberImportActionType.DEACTIVATE) {
      this.deactivateMember(memberWithActions, importAction);
    } else if (importAction.actionType === MemberImportActionType.MODIFY) {
      this.modifyMember(memberWithActions, importAction);
    } else if (importAction.actionType === MemberImportActionType.ADD) {
      this.addMember(memberWithActions, importAction);
    }
  }

  private addMember(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    const member = importAction.from;
    let that = this;
    this.backendService.addMember(member).then(result => {
      this.setMessage("Added member " + member.first_name + " " + member.last_name, Severity.INFO);
      MemberList.removeImportAction(memberWithActions, importAction);
    }, error => {
      this.setMessage("Error adding member " + member.first_name + " " + member.last_name, Severity.ERROR);
      importAction.clicked = false;
    });
  }

  private activateMember(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    const member = importAction.from;
    let that = this;
    this.backendService.activateMember(member, Util.currentDateIso()).then(result => {
      this.setMessage("Activated member " + member.first_name + " " + member.last_name, Severity.INFO);
      MemberList.removeImportAction(memberWithActions, importAction);
    }, error => {
      this.setMessage("Error activating member " + member.first_name + " " + member.last_name, Severity.ERROR);
      importAction.clicked = false;
    });
  }

  private static removeImportAction(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    let index = memberWithActions.importActions.indexOf(importAction);
    if (index !== -1) {
      memberWithActions.importActions.slice(index);
    }
  }

  private deactivateMember(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    const member = importAction.from;
    let that = this;
    this.backendService.deactivateMember(member, Util.currentDateIso()).then(result => {
      this.setMessage("Deactivated member " + member.first_name + " " + member.last_name, Severity.INFO);
      MemberList.removeImportAction(memberWithActions, importAction);
    }, error => {
      this.setMessage("Error deactivating member " + member.first_name + " " + member.last_name, Severity.ERROR);
      importAction.clicked = false;
    });
  }

  private modifyMember(memberWithActions: MemberWithActions, importAction: MemberImportAction) {
    const from = importAction.from;
    const to = importAction.to;
    let that = this;
    this.backendService.modifyMember(from, to).then(result => {
      this.setMessage("Modified member " + from.first_name + " " + from.last_name, Severity.INFO);
      MemberList.removeImportAction(memberWithActions, importAction);
    }, error => {
      console.log("error: " + JSON.stringify(error));
      this.setMessage("Error modifying member " + from.first_name + " " + from.last_name, Severity.ERROR);
      importAction.clicked = false;
    });
  }

  sectionNameFor(member: Member): string {
    let section = this.sections.find(s => s.id == member.section_id);
    return section ? section.name : '(unknown)';
  }

  sectionNameForId(sectionId: number): string {
    let section = this.sections.find(s => s.id == sectionId);
    return section ? section.name : '(unknown)';
  }

  isActive(member: Member): boolean {
    return member.date_to === undefined && member.date_changed === undefined;
  }

  getDateFromIsoStr(dateIsoStr: string): string {
    return Util.isoStrToDdMMYYYY(dateIsoStr)
  }
}

export class KeysValueConverter {
  toView(obj) {
    return Reflect.ownKeys(obj);
  }
}
