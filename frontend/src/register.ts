import {inject} from 'aurelia-framework';
import {Rehearsal, RehearsalWs} from "./model/rehearsal";
import {SectionContact} from "./model/section-contact";
import {AttendanceForRehearsal} from "./model/attendance-for-rehearsal";
import {AttendanceOption} from "./model/attendance-option";
import {declarePropertyDependencies} from 'aurelia-framework';
import {BackendService} from "./backend-service";
import {Util} from "./util";
import * as moment from "moment";
import {MdToastService} from "aurelia-materialize-bridge";
import {I18N} from "aurelia-i18n";

@inject(I18N, BackendService, MdToastService)
export class Register {
  sectionContacts: SectionContact[] = [];
  currentSectionContact: SectionContact = null;
  sectionContactsVisible: boolean = true;

  rehearsals: Rehearsal[] = [];
  currentRehearsal: Rehearsal = null;

  attendance: AttendanceForRehearsal;
  attendanceSelected: number[] = [];
  attendanceOptions: AttendanceOption[] = [
    {present: 1, name: this.i18n.tr('register:present')},
    {present: 0, name: this.i18n.tr('register:absent')},
    {present: 2, name: this.i18n.tr('register:absent_notified')}
  ];

  reloadStaticDataTimeoutMs: number = 1000*60*60; // 1 hour
  reloadRehearsalsTimer: any;

  loadingAttendance: boolean = false;

  constructor(public i18n: I18N, private backendService: BackendService, private toast: MdToastService) {
    this.i18n = i18n;
    this.i18n
      .setLocale('no')
      .then( () => {
        // locale is loaded
      });
    this.toast = toast;
    this.fetchStaticData();
    this.startReloadTimer();
  }

  private startReloadTimer() {
    this.reloadRehearsalsTimer = setInterval(() => {
      this.fetchStaticData();
    }, this.reloadStaticDataTimeoutMs);
  }

  private fetchStaticData() {
    this.fetchAndSortSectionContacts();
    this.fetchAndSortRehearsals();
  }

  private fetchAndSortSectionContacts() {
    let promise = this.backendService.getSectionContacts();
    let that = this;
    Promise.resolve(promise).then(this.initSectionContacts.bind(this), function (e) {
      that.toast.show("Error fetching section contacts", 4000);
    });
  }

  private initSectionContacts(sectionContacts: SectionContact[]) {
    this.sectionContacts = sectionContacts.sort((v1, v2) => v1.section_id - v2.section_id);
  }

  onChangeSectionContact() {
    if (this.currentRehearsal !== null) {
      this.fetchAttendance(this.currentSectionContact, this.currentRehearsal)
    }
    this.toggleSectionContacts();
  }

  sectionNameList(sectionContact: SectionContact) {
    return sectionContact.sections.map(s => s.name).join(", ");
  }

  private fetchAndSortRehearsals() {
    let promise = this.backendService.getRehearsals();
    let that = this;
    Promise.resolve(promise).then(this.initRehearsalsAndCurrentRehearsal.bind(this), function (e) {
      that.toast.show("Error fetching rehearsals", 4000);
    });
  }

  private initRehearsalsAndCurrentRehearsal(rehearsalsWs: RehearsalWs[]) {
    this.rehearsals = rehearsalsWs.sort((r1, r2) => r1.date_start.localeCompare(r2.date_start))
      .map(r => new Rehearsal(r.id, new Date(r.date_start), new Date(r.date_end), r.obligatory, r.information));
    this.currentRehearsal = Util.closestRehearsal(this.rehearsals);
  }

  dateTimeFormatted(date: Date): string {
    return moment(date).format('YYYY-MM-DD HH:mm');
  }

  fetchAttendance(sectionContact: SectionContact, rehearsal: Rehearsal) {
    let promise = this.backendService.getAttendance(sectionContact, rehearsal);
    this.loadingAttendance = true;
    let that = this;
    Promise.resolve(promise).then(this.initAttendanceOrError.bind(this), function (e) {
      that.toast.show("Error fetching attendance", 4000);
    });
  }

  private initAttendanceOrError(attendance: AttendanceForRehearsal[]) {
    if (attendance.length > 0) {
      this.attendance = attendance[0];
      Util.sortAttendanceByName(this.attendance);
      this.attendanceSelected = this.attendance.for_members.map(fm => fm.present)
    } else {
      this.toast.show("Error fetching attendance", 4000);
    }
    this.loadingAttendance = false;
  }

  get existsPrevious(): boolean {
    return this.currentIndex() - 1 >= 0
  }

  previousRehearsal(): void {
    if (this.existsPrevious) {
      this.currentRehearsal = this.rehearsals[this.currentIndex() - 1];
      this.fetchAttendance(this.currentSectionContact, this.currentRehearsal)
    }
  }

  get existsNext(): boolean {
    return this.currentIndex() + 1 < this.rehearsals.length
  }

  nextRehearsal(): void {
    if (this.existsNext) {
      this.currentRehearsal = this.rehearsals[this.currentIndex() + 1];
      this.fetchAttendance(this.currentSectionContact, this.currentRehearsal)
    }
  }

  currentIndex(): number {
    return this.rehearsals.indexOf(this.currentRehearsal)
  }

  save() {
    this.attendanceSelected.map((present, index) => this.attendance.for_members[index].present = present);
    let promise = this.backendService.saveAttendance(this.currentSectionContact, this.currentRehearsal, this.attendance);
    let that = this;
    Promise.resolve(promise).then(function (response) {
      that.toast.show("Attendance has been saved", 4000);
    }, function (e) {
      that.toast.show("Error saving attendance", 4000);
    });
  }

  toggleSectionContacts() {
    this.sectionContactsVisible = !this.sectionContactsVisible;
  }
}

declarePropertyDependencies(Register, 'existsPrevious', ['currentRehearsal']);
declarePropertyDependencies(Register, 'existsNext', ['currentRehearsal']);
