import {Rehearsal} from "./model/rehearsal";
import {AttendanceForRehearsal} from "./model/attendance-for-rehearsal";
import * as moment from "moment";

export class Util {

  static currentDateIso() {
    return new Date().toISOString();
  }

  static isoStrToDdMM(localtimedate_str: string) {
    if (localtimedate_str === undefined || localtimedate_str === null) {
      return localtimedate_str;
    } else {
      const localdate_str = localtimedate_str.split("T")[0];
      const date_parts = localdate_str.split("-");
      return date_parts[2] + "." + date_parts[1]
    }
  }

  static isoStrToDdMMYYYY(localtimedate_str: string) {
    if (localtimedate_str === undefined || localtimedate_str === null) {
      return localtimedate_str;
    } else {
      const localdate_str = localtimedate_str.split("T")[0];
      const date_parts = localdate_str.split("-");
      return date_parts[2] + "." + date_parts[1] + "." + date_parts[0]
    }
  }

  static closestRehearsal(rehearsalsSortedAscending: Rehearsal[]): Rehearsal {
    if (rehearsalsSortedAscending.length == 0) {
      return undefined
    } else {
      const nowEpochMs = new Date().getTime();
      let diffMap: Map<Rehearsal, number> = new Map();
      rehearsalsSortedAscending.map(r => {
        const epochMs = new Date(r.date_start).getTime();
        const diff = Math.abs(epochMs - nowEpochMs);
        diffMap.set(r, diff)
      });
      let closest = undefined;
      diffMap.forEach((diff, r) => {
        if (closest === undefined) {
          closest = r
        } else {
          if (diff < diffMap.get(closest)) {
            closest = r
          }
        }
      });
      return closest
    }
  }

  static sortAttendanceByName(attendance: AttendanceForRehearsal) {
    attendance.members.sort((m1, m2) => {
      const name1 = m1.first_name + m1.last_name;
      const name2 = m2.first_name + m2.last_name;
      return name1.localeCompare(name2);
    });
    attendance.for_members.sort((fm1, fm2) => {
      const m1Index = attendance.members.indexOf(attendance.members.find(m => m.member_id == fm1.member_id));
      const m2Index = attendance.members.indexOf(attendance.members.find(m => m.member_id == fm2.member_id));
      return m1Index - m2Index
    });
  }

  static nextSemesterStartDate(date: Date): Date {
    if (this.isSpringHalfyear(date)) {
      return new Date(date.getFullYear() + "-08-15");
    } else {
      return new Date(date.getFullYear() + 1 + "-01-01");
    }
  }

  static semesterEndDate(date: Date): Date {
    if (this.isSpringHalfyear(date)) {
      return new Date(date.getFullYear() + "-06-30");
    } else {
      return new Date(date.getFullYear() + "-12-23");
    }
  }

  static isSpringHalfyear(date: Date): boolean {
    //return date.getMonth() < 7 || (date.getMonth() === 5 && date.getDate() < 30);
    return date.getMonth() < 7 || (date.getMonth() === 7 && date.getDate() < 15);
  }

  static isAutumnHalfyear(date: Date): boolean {
    //return (date.getMonth() > 7 || (date.getMonth() === 7 && date.getDate() >= 15)) && !(date.getMonth() === 11 && date.getDate() >= 24);
    return date.getMonth() > 7 || (date.getMonth() === 7 && date.getDate() >= 15);
  }

  static isLastRehearsalInSemester(rehearsal: Rehearsal, rehearsals: Rehearsal[], orderAscending: boolean): boolean {
    const nextSemesterStartDate = Util.nextSemesterStartDate(rehearsal.date_end);
    const nextRehearsal: Rehearsal = orderAscending ? Util.findNextRehearsal(rehearsal, rehearsals) : Util.findPreviousRehearsal(rehearsal, rehearsals);
    return nextRehearsal === undefined || (nextRehearsal && nextRehearsal.date_start >= nextSemesterStartDate);
  }

  static findNextRehearsal(rehearsal: Rehearsal, rehearsals: Rehearsal[]): Rehearsal {
    const indexOnList: number = rehearsals.findIndex(r => r.id === rehearsal.id);
    if (indexOnList !== -1 && indexOnList < rehearsals.length - 1) {
      return rehearsals[indexOnList + 1];
    } else {
      return undefined;
    }
  }

  static findPreviousRehearsal(rehearsal: Rehearsal, rehearsals: Rehearsal[]): Rehearsal {
    const indexOnList: number = rehearsals.findIndex(r => r.id === rehearsal.id);
    if (indexOnList !== -1 && indexOnList > 0) {
      return rehearsals[indexOnList - 1];
    } else {
      return undefined;
    }
  }

  static formatDateAndStripZone(date: Date): string {
    return moment(date).format().split('+')[0];
  }

  static getTimeFromDate(date: Date): string {
    return Util.pad(date.getHours().toString(), 2) + ":" + Util.pad(date.getMinutes().toString(), 2);
  }

  static pad(value: string, length: number): string {
    return (value.toString().length < length) ? Util.pad("0"+value, length) : value;
  }
}
