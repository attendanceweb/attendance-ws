import {Member} from "../model/member";
import {MemberImportAction, MemberImportActionType} from "./member-import-action";
import {MemberWithActions, MemberActionsForSection} from "./member-with-actions";

class MemberPair {
  constructor(public first: Member, public second: Member) {
  }
}

export class MemberlistChangeProcessor {

  static initMembersWithActions(source: Member[], destination: Member[]): MemberActionsForSection[] {
    let actions: MemberImportAction[] = MemberlistChangeProcessor.createMemberImportActions(source, destination);
    return MemberlistChangeProcessor.sortAndGroupActionsBySection(destination, actions);
  }

  private static createMemberImportActions(source: Member[], destination: Member[]): MemberImportAction[] {
    const membersToAdd: Member[] = MemberlistChangeProcessor.findMembersNotPresentInDestination(source, destination);
    const memberAddActions: MemberImportAction[] = membersToAdd.map(m => new MemberImportAction(MemberImportActionType.ADD, false, m, null));

    const membersToActivate: Member[] = MemberlistChangeProcessor.findMembersPresentButNotActiveInDestination(source, destination);
    const memberActivateActions: MemberImportAction[] = membersToActivate.map(m => new MemberImportAction(MemberImportActionType.ACTIVATE, false, m, null));

    const membersToModify: MemberPair[] = MemberlistChangeProcessor.findModifiedMembers(source, destination);
    const memberModifyActions: MemberImportAction[] = membersToModify.map(mp => new MemberImportAction(MemberImportActionType.MODIFY, false, mp.first, mp.second));

    const membersToDeactivate: Member[] = MemberlistChangeProcessor.findMembersNotPresentInSource(source, destination);
    const memberDeactivateActions: MemberImportAction[] = membersToDeactivate.map(m => new MemberImportAction(MemberImportActionType.DEACTIVATE, false, m, null));

    let actions: MemberImportAction[] = memberAddActions;
    actions.push(...memberActivateActions);
    actions.push(...memberModifyActions);
    actions.push(...memberDeactivateActions);
    return actions;
  }

  static findMembersNotPresentInDestination(source: Member[], destination: Member[]): Member[] {
    return source
      .filter(m => MemberlistChangeProcessor.findIn(m, destination).length === 0);
  }

  static findMembersPresentButNotActiveInDestination(source: Member[], destination: Member[]): Member[] {
    return source
      .filter(m => {
        const inDst: Member[] = MemberlistChangeProcessor.findIn(m, destination);
        const inDstActive: Member[] = inDst.filter(m2 => m2.date_changed === undefined && m2.date_to === undefined);
        return inDst.length > 0 && inDstActive.length === 0;
      });
  }

  static findMembersNotPresentInSource(source: Member[], destination: Member[]): Member[] {
    return destination
      .filter(m => m.date_changed === undefined && m.date_to === undefined)
      .filter(m => MemberlistChangeProcessor.findIn(m, source).length === 0);
  }

  static findModifiedMembers(source: Member[], destination: Member[]): MemberPair[] {
    return source
      .map(memberSrc => {
        const maybeModifiedMember = MemberlistChangeProcessor.findIn(memberSrc, destination)
          .filter(memberDst => memberDst.date_changed === undefined && memberDst.date_to === undefined)
          .find(memberDst => !MemberlistChangeProcessor.haveSameNamesAndSection(memberSrc, memberDst));
        if (maybeModifiedMember !== undefined) {
          return new MemberPair(maybeModifiedMember, memberSrc);
        }
      }).filter(mp => mp !== undefined);
  }

  private static findIn(member: Member, members: Member[]): Member[] {
    return members.filter(other => other.member_id == member.member_id);
  }

  private static haveSameNamesAndSection(member: Member, other: Member): boolean {
    return member.first_name === other.first_name &&
      member.last_name === other.last_name &&
      member.section_id === other.section_id;
  }

  private static sortAndGroupActionsBySection(destination: Member[], actions: MemberImportAction[]): MemberActionsForSection[] {
    let membersWithActions: MemberWithActions[] = destination.map(m => new MemberWithActions(m, []));

    actions.forEach(action => {
      let memberWithActions: MemberWithActions = membersWithActions.find(ma => ma.member.member_id === action.from.member_id);
      if (memberWithActions === undefined) {
        memberWithActions = new MemberWithActions(action.from, []);
        membersWithActions.push(memberWithActions);
      }
      memberWithActions.importActions.push(action);
    });

    let forSections: MemberActionsForSection[] = [];
    membersWithActions
      .sort((m1, m2) => MemberlistChangeProcessor.memberCompareTo(m1.member, m2.member))
      .forEach(action => {
        const key = action.member.section_id;
        let actions: MemberActionsForSection = forSections.find(m => m.sectionId === key);
        if (actions === undefined) {
          actions = new MemberActionsForSection(key, []);
          forSections.push(actions);
        }
        actions.membersWithActions.push(action);
      });

    return forSections.sort((a1, a2) => a1.sectionId - a2.sectionId);
  }

  private static memberCompareTo(m1: Member, m2: Member): number {
    return (m1.first_name + m1.last_name).localeCompare(m2.first_name + m2.last_name);
  }
}
