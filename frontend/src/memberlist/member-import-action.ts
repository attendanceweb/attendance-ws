import {Member} from "../model/member";

export enum MemberImportActionType {
  ADD, ACTIVATE, DEACTIVATE, MODIFY
}

export class MemberImportAction {
  constructor(public actionType: MemberImportActionType,
              public clicked: boolean,
              public from?: Member,
              public to?: Member) {
  }
}
