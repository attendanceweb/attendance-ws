/// <reference path="../../node_modules/@types/gapi/index.d.ts" />

declare var gapi: any;

export class GoogleSheetsService {

  // Array of API discovery doc URLs for APIs used by the quickstart
  DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];

  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  SCOPES = "https://www.googleapis.com/auth/spreadsheets.readonly";

  signedIn: boolean;

  init(onSuccess: (result: any) => any, onLoadFailure: (err: any) => any, onInitFailure: (err2: any) => any) {
    this.loadClient().then(result => {
        this.initClient().then(result => {
          onSuccess(result);
        }, err => {
          onInitFailure(err);
        });
      },
      err2 => {
        onLoadFailure(err2);
      }
    );
  }

  private loadClient(): Promise<any> {
    return new Promise((resolve, reject) => {
      gapi.load('client', {
        callback: resolve,
        onerror: reject,
        timeout: 1000, // 5 seconds.
        ontimeout: reject
      });
    });
  }

  private initClient(): Promise<any> {
    let initObj = {
      apiKey: process.env.GOOGLE_API_KEY,
      clientId: (process.env.GOOGLE_PUBLIC === "true") ? undefined : process.env.GOOGLE_CLIENT_ID,
      discoveryDocs: this.DISCOVERY_DOCS,
      scope: (process.env.GOOGLE_PUBLIC) ? undefined : this.SCOPES
    };

    let initPromise = gapi.client.init(initObj);

    if (!process.env.GOOGLE_PUBLIC) {
      return initPromise.then(result => {
        this.arrayForEachFix();

        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(this.updateSigninStatus.bind(this));

        // Handle the initial sign-in state.
        this.updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      });
    } else {
      this.arrayForEachFix();
      return initPromise;
    }
  }

  // Borrowed from https://stackoverflow.com/questions/43040405/loading-aurelia-breaks-google-api
  private arrayForEachFix() {
    const originTest = RegExp.prototype.test;
    RegExp.prototype.test = function test(v: any) {
      if (typeof v === 'function' && v.toString().includes('__array_observer__.addChangeRecord')) {
        return true;
      }
      return originTest.apply(this, arguments);
    };
  }

  private updateSigninStatus(isSignedIn: boolean): void {
    this.signedIn = isSignedIn;
  }

  signIn() {
    gapi.auth2.getAuthInstance().signIn();
  }

  signOut() {
    gapi.auth2.getAuthInstance().signOut();
  }

  getValues(): Promise<any> {
    return gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: process.env.GOOGLE_SPREADSHEET_ID,
      range: process.env.GOOGLE_SPREADSHEET_RANGE
    });
  }
}
