import {MemberImportAction} from "./member-import-action";
import {Section} from "../model/section";
import {Member} from "../model/member";

export class MemberWithActions {
  constructor(public member: Member,
              public importActions: MemberImportAction[]) {}
}

export class MemberActionsForSection {
  constructor(public sectionId: number,
              public membersWithActions: MemberWithActions[]) {}
}
