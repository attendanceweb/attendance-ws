import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';

@inject(HttpClient)
export class CrudApi {

  constructor(private http: HttpClient, baseUrl: string) {
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl(baseUrl)
        .withDefaults({
          headers: {
            'content-type': 'application/json',
            'Accept': 'application/json',
            'X-Requested-With': 'Fetch'
          }
        })
    });

    this.http = http;
  }

  query(service: string): Promise<any> {
    return this.http.fetch(`${service}`)
      .then(response => response.json());
  }

  one(service: string, id: string): Promise<any> {
    return this.http.fetch(`${service}/${id}`)
      .then(response => response.json());
  }

  create(service: string, item): Promise<any> {
    return this.http.fetch(`${service}`, {
      method: 'post',
      body: json(item)
    }).then(response => response.json());
  }

  post(service: string, item): Promise<any> {
    return this.http.fetch(`${service}`, {
      method: 'post',
      body: json(item)
    }).then(response => response.json());
  }
  
  update(service: string, id: string, item): Promise<any> {
    return this.http.fetch(`${service}/${id}`, {
      method: 'put',
      body: json(item)
    }).then(response => response.json());
  }

  delete(service: string, id: string): Promise<any> {
    return this.http.fetch(`${service}/${id}`, {
      method: 'delete'
    }).then(response => response.json());
  }

  queryNested(service: string, id: string, resource): Promise<any> {
    return this.http.fetch(`${service}/${id}/${resource}`)
      .then(response => response.json());
  }
}
