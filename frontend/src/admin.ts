import {inject} from 'aurelia-framework';
import {BackendService} from "./backend-service";
import {Rehearsal, RehearsalToEdit, RehearsalWs} from "./model/rehearsal";
import {PageWithMessage} from "./page-with-message";
import {Severity} from "./message";
import {Util} from "./util";
import * as moment from 'moment';
import {ConfigParam} from "./model/configuration";
import {Member} from "./model/member";

@inject(BackendService)
export class Admin extends PageWithMessage {

  rehearsals: RehearsalToEdit[] = [];
  editedRehearsal: RehearsalToEdit = undefined;

  configParams: ConfigParam[] = [];

  membersInactiveForOverAYear: Member[] = [];
  
  constructor(private backendService: BackendService) {
    super();
    this.fetchRehearsals();
    this.fetchConfiguration();
    this.fetchMembersInactiveForOverAYear();
  }

  private fetchRehearsals() {
    this.resetMessage();
    let promise = this.backendService.getRehearsals();
    let that = this;
    Promise.resolve(promise).then(this.initRehearsals.bind(this), function (e) {
      that.setMessage("Error fetching rehearsals", Severity.ERROR);
    });
  }

  private initRehearsals(rehearsalsWs: RehearsalWs[]) {
    this.rehearsals = rehearsalsWs.sort((r1, r2) => (-1) * r1.date_start.localeCompare(r2.date_start))
      .map(r => {
        let dateStart = new Date(r.date_start);
        let dateEnd = new Date(r.date_end);
        let timeStart = Util.getTimeFromDate(dateStart);
        let timeEnd = Util.getTimeFromDate(dateEnd);
        return new RehearsalToEdit(r.id, dateStart, timeStart, dateEnd, timeEnd, r.obligatory, r.information);
      });
  }

  addRehearsal() {
    let now = new Date();
    let rehearsal = new RehearsalToEdit(-1, now, "18:30", now, "21:00", true, null);
    this.rehearsals.unshift(rehearsal);
    this.editRehearsal(rehearsal);
  }

  saveRehearsal(rehearsal: RehearsalToEdit) {
    rehearsal.date_start = this.editedRehearsal.date_start;
    rehearsal.time_start = this.editedRehearsal.time_start;
    rehearsal.date_end = this.editedRehearsal.date_end;
    rehearsal.time_end = this.editedRehearsal.time_end;
    rehearsal.obligatory = this.editedRehearsal.obligatory;
    rehearsal.information = this.editedRehearsal.information;
    this.saveInWs(this.editedRehearsal);
  }

  private saveInWs(rehearsalToEdit: RehearsalToEdit) {
    let timeStartArr: string[] = rehearsalToEdit.time_start.split(":");
    let timeEndArr: string[] = rehearsalToEdit.time_end.split(":");
    let dateStart2: Date = new Date(rehearsalToEdit.date_start.getFullYear(), rehearsalToEdit.date_start.getMonth(), rehearsalToEdit.date_start.getDate(), Number(timeStartArr[0]), Number(timeStartArr[1]));
    let dateEnd2: Date = new Date(rehearsalToEdit.date_end.getFullYear(), rehearsalToEdit.date_end.getMonth(), rehearsalToEdit.date_end.getDate(), Number(timeEndArr[0]), Number(timeEndArr[1]));
    let rehearsal = new Rehearsal(rehearsalToEdit.id, dateStart2, dateEnd2, rehearsalToEdit.obligatory, rehearsalToEdit.information);
    let promise = this.backendService.saveRehearsal(rehearsal);
    let that = this;
    Promise.resolve(promise).then(ok => {
      if (rehearsal.id === -1) {
        that.setMessage("Added rehearsal", Severity.INFO);
        // TODO for some reason doing it here makes promise resolution hang
        //that.endRehearsalEdit();
        rehearsal.id = ok.id;
      } else {
        that.setMessage("Modfied rehearsal", Severity.INFO);
      }
    }, function (e) {
      that.setMessage("Error saving rehearsal", Severity.ERROR);
    });

    this.endRehearsalEdit();
  }

  cancelRehearsalEdit() {
    if (this.editedRehearsal.id === -1) {
      this.removeRehearsalFromList(this.editedRehearsal.id);
    }
    this.endRehearsalEdit();
  }

  private removeRehearsalFromList(rehearsalIndex: number) {
    const indexOnList: number = this.rehearsals.findIndex(r => r.id === rehearsalIndex);
    if (indexOnList !== -1) {
      this.rehearsals.splice(indexOnList, 1);
    }
  }

  endRehearsalEdit() {
    this.editedRehearsal = undefined;
  }

  editRehearsal(rehearsal: RehearsalToEdit) {
    if (this.editedRehearsal !== undefined) {
      this.cancelRehearsalEdit();
    }
    let dateStart = new Date(rehearsal.date_start);
    let dateEnd = new Date(rehearsal.date_end);
    this.editedRehearsal = new RehearsalToEdit(rehearsal.id, dateStart, rehearsal.time_start, dateEnd, rehearsal.time_end, rehearsal.obligatory, rehearsal.information);
  }

  addRehearsalsInNextSemester() {
    const firstDate = this.rehearsals.length > 0 ? this.rehearsals[0].date_start : new Date();
    const semesterStartDate = Util.nextSemesterStartDate(firstDate);
    const semesterEndDate = Util.semesterEndDate(semesterStartDate);
    const rehearsalWeekday = 1;
    const rehearsalHours = 18;
    const rehearsalMinutes = 30;
    const startTime = "18:30";
    const startDates: Date[] = this.buildRehearsalStartDatesBetween(semesterStartDate, semesterEndDate, rehearsalWeekday, rehearsalHours, rehearsalMinutes);

    const rehearsalDurationHours = 2;
    const rehearsalDurationMinutes = 30;
    const endTime = "21:00";
    const rehearsals: RehearsalToEdit[] = startDates.map(d => {
      const endDate: Date = moment(d).add(rehearsalDurationHours, 'hours').add(rehearsalDurationMinutes, 'minutes').toDate();
      return new RehearsalToEdit(-1, d, startTime, endDate, endTime, true, "");
    });

    rehearsals.forEach(r => {
      const that = this;
      this.backendService.saveRehearsal(r).then(response => {
        if (r.id === -1) {
          r.id = response.id;
        }
        // TODO handle errors here, e.g. mark the rehearsal as failed and give the user an opportunity to retry
      });
      that.rehearsals.unshift(r);
    });
  }

  private buildRehearsalStartDatesBetween(start: Date, end: Date, rehearsalWeekday: number, rehearsalHours: number, rehearsalMinutes: number): Date[] {
    const startMoment: moment.Moment = moment(start).startOf('day');
    const semester2StartDateMoment: moment.Moment = moment(end).startOf('day');

    let currentMoment = moment(start).startOf('isoWeek').add(rehearsalWeekday, 'days');
    if (currentMoment.isBefore(startMoment)) {
      currentMoment.add(1, 'weeks');
    }

    let rehearsalStartDates: Date[] = [];
    while (currentMoment.isBefore(semester2StartDateMoment)) {
      const startDate: Date = currentMoment.clone().add(rehearsalHours, 'hours').add(rehearsalMinutes, 'minutes').toDate();
      rehearsalStartDates.push(startDate);
      currentMoment.add(1, 'weeks');
    }

    return rehearsalStartDates;
  }

  isLastRehearsalInSemester(rehearsal: Rehearsal): boolean {
    return Util.isLastRehearsalInSemester(rehearsal, this.rehearsals, false);
  }

  dateTimeFormatted(date: Date): string {
    return moment(date).format('YYYY-MM-DD HH:mm');
  }

  removeRehearsal(rehearsal: RehearsalToEdit) {
    this.backendService.removeRehearsal(rehearsal.id).then(response => {
      // TODO handle error
      const index = this.rehearsals.indexOf(rehearsal);
      if (index !== -1) {
        this.rehearsals.splice(index, 1);
      }
    });
  }

  private fetchConfiguration() {
    let promise = this.backendService.getConfiguration();
    this.resetMessage();
    let that = this;
    Promise.resolve(promise).then(this.initConfiguration.bind(this), function (e) {
      that.setMessage("Error fetching data from the server", Severity.ERROR);
    });
  }

  private initConfiguration(configParams: ConfigParam[]) {
    this.configParams = configParams;
  }

  private fetchMembersInactiveForOverAYear() {
    let promise = this.backendService.getMembersInactiveForOverAYear();
    this.resetMessage();
    let that = this;
    Promise.resolve(promise).then(this.initMembersInactiveForOverAYear.bind(this), function (e) {
      that.setMessage("Error fetching data from the server", Severity.ERROR);
    });
  }

  private initMembersInactiveForOverAYear(members: Member[]) {
    this.membersInactiveForOverAYear = members;
  }
  
  cleanupMembersInactiveForOverAYear() {
    this.backendService.cleanupMembersInactiveForOverAYear();
  }

}

// tslint:disable-next-line:max-classes-per-file
export class StringifyValueConverter {
  toView(value) {
    return JSON.stringify(value);
  }
}
