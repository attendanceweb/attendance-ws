import {CrudApi} from "./crud-api";
import {SectionContact} from "./model/section-contact";
import {Rehearsal, RehearsalWs} from "./model/rehearsal";
import {AttendanceForRehearsal} from "./model/attendance-for-rehearsal";
import {HttpClient} from "aurelia-fetch-client";
import {Member} from "./model/member";
import {
  MemberActivateDeactivateResponse,
  MemberActivateRequest,
  MemberAddRequest, MemberAddResponse,
  MemberDeactivateRequest,
  MemberModifyRequest,
  MemberModifyResponse
} from "./model/member-operations";
import {Util} from "./util";
import {ConfigParam} from "./model/configuration";

export class BackendService {

  backendUrl = process.env.BACKEND_URL ? process.env.BACKEND_URL : "/api/";

  private crudApi: CrudApi = new CrudApi(new HttpClient, this.backendUrl);

  getSections(): Promise<any> {
    return this.crudApi.query("sections");
  }

  getSectionContacts(): Promise<any> {
    return this.crudApi.query("sectioncontacts");
  }

  getRehearsals(): Promise<any> {
    return this.crudApi.query("rehearsals");
  }

  saveRehearsal(rehearsal: Rehearsal): Promise<any> {
    let rehearsalWs = new RehearsalWs(rehearsal.id, Util.formatDateAndStripZone(rehearsal.date_start), Util.formatDateAndStripZone(rehearsal.date_end), rehearsal.obligatory, rehearsal.information);
    if (rehearsalWs.id === -1) {
      return this.addRehearsal(rehearsalWs);
    } else {
      return this.updateRehearsal(rehearsalWs);
    }
  }

  private addRehearsal(rehearsalWs: RehearsalWs): Promise<any> {
    return this.crudApi.create("rehearsals", rehearsalWs);
  }

  private updateRehearsal(rehearsalWs: RehearsalWs): Promise<any> {
    return this.crudApi.update("rehearsals", rehearsalWs.id.toLocaleString(), rehearsalWs);
  }

  removeRehearsal(rehearsalId: number): Promise<any> {
    return this.crudApi.delete("rehearsals", rehearsalId.toString());
  }

  saveAttendance(sectionContact: SectionContact, rehearsal: Rehearsal, attendance: AttendanceForRehearsal): Promise<any> {
    let url = "attendance/" + sectionContact.member_id + "/" + rehearsal.id;
    return this.crudApi.create(url, attendance);
  }

  getAttendance(sectionContact: SectionContact, rehearsal: Rehearsal) {
    const sectionContactId = sectionContact === null ? "" : sectionContact.member_id;
    return this.crudApi.query("attendance/" + sectionContactId + "/" + rehearsal.id);
  }

  getMembers() {
    return this.crudApi.query("members");
  }

  getAbsence(dateFrom: string = null, dateTo: string = null) {
    let dateFromElement = (dateFrom !== null) ? "/" + dateFrom : "";
    let dateToElement = (dateFrom !== null && dateTo !== null) ? "/" + dateTo : "";
    return this.crudApi.query("absence" + dateFromElement + dateToElement);
  }

  addMember(member: Member): Promise<MemberAddResponse> {
    let request = new MemberAddRequest();
    request.member_id = member.member_id;
    request.first_name = member.first_name;
    request.last_name = member.last_name;
    request.section_id = member.section_id;
    request.date_from = member.date_from;
    return this.crudApi.create("members", request);
  }

  activateMember(member: Member, dateFrom: string): Promise<MemberActivateDeactivateResponse> {
    let request = new MemberActivateRequest();
    request.date_from = dateFrom;
    return this.crudApi.update("members/activate", member.member_id.toLocaleString(), request);
  }

  deactivateMember(member: Member, dateTo: string): Promise<MemberActivateDeactivateResponse> {
    let request = new MemberDeactivateRequest();
    request.date_to = dateTo;
    return this.crudApi.update("members/deactivate", member.member_id.toLocaleString(), request);
  }

  modifyMember(from: Member, to: Member): Promise<MemberModifyResponse> {
    let request = new MemberModifyRequest();
    request.first_name = to.first_name;
    request.last_name = to.last_name;
    request.section_id = to.section_id;
    return this.crudApi.update("members", from.member_id.toLocaleString(), request);
  }

  getConfiguration(): Promise<ConfigParam[]> {
    return this.crudApi.query("config");
  }

  getMembersInactiveForOverAYear(): Promise<Member[]> {
    return this.crudApi.query("members/cleanup");
  }

  cleanupMembersInactiveForOverAYear(): void {
    this.crudApi.post("members/cleanup", {});
  }
}
