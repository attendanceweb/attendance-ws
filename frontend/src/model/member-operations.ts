export class MemberAddRequest {
  member_id: number;
  first_name: string;
  last_name: string;
  section_id: number;
  date_from: string;
}

export class MemberAddResponse {
  status: string;
  message: string;
}

export class MemberModifyRequest {
  first_name: string;
  last_name: string;
  section_id: number;
}

export class MemberModifyResponse {
  status: string;
  message: string;
}

export enum MemberModificationStatus {
  Ok, MemberNotFound, NoChanges, AddingFailed
}

export class MemberActivateRequest {
  date_from: string;
}

export class MemberDeactivateRequest {
  date_to: string;
}

export class MemberActivateDeactivateResponse {
  status: string;
  message: string;
}

export enum MemberActivateStatus {
  Activated, OverlappingActivityPeriods, AlreadyActive,
    Deactivated, AlreadyInactive,
    ChangedActivityPeriod, ChangeWouldOrphanAttendance, ActivityNotFound,
    MemberNotFound,
    Invalid
}
