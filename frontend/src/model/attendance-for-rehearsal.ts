import {Member} from "./member";

export class AttendanceForRehearsal {
  rehearsal_id: number;
  members: Member[];
  for_members: AttendanceForMember[];
}

export class AttendanceForMember {
  member_id: number;
  present: number;
}
