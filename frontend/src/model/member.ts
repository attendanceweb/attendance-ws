export class Member {
  member_id: number;
  first_name: string;
  last_name: string;
  section_id: number;
  date_from: string;
  date_to?: string;
  date_changed?: string;
}
