export class RehearsalWs {
  constructor(public id: number,
              public date_start: string,
              public date_end: string,
              public obligatory: boolean,
              public information: string) {
  }
}

export class Rehearsal {
  constructor(public id: number,
              public date_start: Date,
              public date_end: Date,
              public obligatory: boolean,
              public information: string) {
  }
}

export class RehearsalToEdit {
  constructor(public id: number,
              public date_start: Date,
              public time_start: string,
              public date_end: Date,
              public time_end: string,
              public obligatory: boolean,
              public information: string) {
  }

  isEqual(other: Rehearsal): boolean {
    return this.id == other.id;
  }
}
