import {SectionContact} from "./section-contact";

export class MemberAbsenceForSectionContact {
  sectionContact: SectionContact;
  membersAbsence: MemberAbsence[];
}

export class MemberAbsence {
  member_id: number;
  first_name: string;
  last_name: string;
  section_id: number;
  absence_count: number;
  active: boolean;
  absence: MemberAbsenceForProject[];
}

export class MemberAbsenceForProject {
  project: Project;
  absence: MemberAttendance[];
}

export class Project {
  id: number;
  name: string;
  startEnd: Interval[];
}

export class Interval {
  start: string;
  end: string;
}

export class MemberAttendance {
  member_id: number;
  first_name: string;
  last_name: string;
  section_id: number;
  rehearsal_id: number;
  rehearsal_date_start: string;
  rehearsal_date_end: string;
  present: number;
}
