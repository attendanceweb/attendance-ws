import {Section} from "./section";

export class SectionContact {
    constructor(public member_id: number,
                public section_id: number,
                public first_name: string,
                public last_name: string,
                public email: string,
                public sections: Section[]) {
    }
}
