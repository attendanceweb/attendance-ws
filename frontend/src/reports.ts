import {inject} from 'aurelia-framework';
import {BackendService} from "./backend-service";
import {Rehearsal, RehearsalWs} from "./model/rehearsal";
import {PageWithMessage} from "./page-with-message";
import {Severity} from "./message";
import {Util} from "./util";
import * as moment from 'moment';
import {MemberAbsenceForSectionContact} from "./model/member-absence";
import {Section} from "./model/section";
import {AttendanceForRehearsal} from "./model/attendance-for-rehearsal";
import {AttendanceOption} from "./model/attendance-option";
import {declarePropertyDependencies} from 'aurelia-framework';
import {I18N} from "aurelia-i18n";

@inject(I18N, BackendService)
export class Reports extends PageWithMessage {

  /* Absence report */
  absence: MemberAbsenceForSectionContact[] = [];

  /* Attendance report */
  rehearsals: Rehearsal[] = [];
  currentRehearsal: Rehearsal = null;

  attendance: AttendanceForRehearsal;
  attendanceSelected: number[] = [];
  attendanceOptions: AttendanceOption[] = [
    {present: 1, name: this.i18n.tr('register:present')},
    {present: 0, name: this.i18n.tr('register:absent')},
    {present: 2, name: this.i18n.tr('register:absent_notified')}
  ];

  constructor(public i18n: I18N, private backendService: BackendService) {
    super();
    this.i18n = i18n;
    this.i18n
      .setLocale('no')
      .then( () => {
        // locale is loaded
      });
    this.fetchAndSortRehearsals();
    this.fetchAbsence();
  }

  /* Attendance report */
  private fetchAndSortRehearsals() {
    let promise = this.backendService.getRehearsals();
    this.resetMessage();
    let that = this;
    Promise.resolve(promise).then(this.initRehearsalsAndAttendance.bind(this), function (e) {
      that.setMessage("Error fetching rehearsals", Severity.ERROR);
    });
  }

  private initRehearsalsAndAttendance(rehearsalsWs: RehearsalWs[]) {
    this.rehearsals = rehearsalsWs.sort((r1, r2) => r1.date_start.localeCompare(r2.date_start))
      .map(r => {
        let dateStart = new Date(r.date_start);
        let dateEnd = new Date(r.date_end);
        return new Rehearsal(r.id, dateStart, dateEnd, r.obligatory, r.information);
      });
    this.currentRehearsal = Util.closestRehearsal(this.rehearsals);
    this.fetchAttendance(this.currentRehearsal);
  }

  dateTimeFormatted(date: Date): string {
    return moment(date).format('YYYY-MM-DD HH:mm');
  }

  fetchAttendance(rehearsal: Rehearsal) {
    let promise = this.backendService.getAttendance(null, rehearsal);
    this.resetMessage();
    let that = this;
    Promise.resolve(promise).then(this.initAttendanceOrError.bind(this), function (e) {
      that.setMessage("Error fetching attendance", Severity.ERROR);
    });
  }

  private initAttendanceOrError(attendance: AttendanceForRehearsal[]) {
    if (attendance.length > 0) {
      this.attendance = attendance[0];
      Util.sortAttendanceByName(this.attendance);
      this.attendanceSelected = this.attendance.for_members.map(fm => fm.present)
    } else {
      this.setMessage("Error fetching attendance", Severity.ERROR);
    }
  }

  get existsPrevious(): boolean {
    return this.currentIndex() - 1 >= 0
  }

  previousRehearsal(): void {
    if (this.existsPrevious) {
      this.currentRehearsal = this.rehearsals[this.currentIndex() - 1];
      this.fetchAttendance(this.currentRehearsal)
    }
  }

  get existsNext(): boolean {
    return this.currentIndex() + 1 < this.rehearsals.length
  }

  nextRehearsal(): void {
    if (this.existsNext) {
      this.currentRehearsal = this.rehearsals[this.currentIndex() + 1];
      this.fetchAttendance(this.currentRehearsal)
    }
  }

  currentIndex(): number {
    return this.rehearsals.indexOf(this.currentRehearsal)
  }

  get attendanceUrl(): string {
    return this.backendService.backendUrl + "attendance/" + this.currentRehearsal.id + ".csv";
  }

  get attendanceSelectedCount(): number {
    return this.attendanceSelected.filter(a => a !== -1).length;
  }

  getPresentLabel(memberPresent: number): string {
    const option = this.attendanceOptions.find(a => a.present === memberPresent);
    if (option !== undefined) {
      return option.name;
    }
  }

  /* Absence report */
  private fetchAbsence() {
    let promise = this.backendService.getAbsence();
    this.resetMessage();
    let that = this;
    Promise.resolve(promise).then(this.initAbsence.bind(this), function (e) {
      that.setMessage("Error fetching absence", Severity.ERROR);
    });
  }

  private initAbsence(absence: MemberAbsenceForSectionContact[]) {
    this.absence = absence;
  }

  getSectionNames(sections: Section[]) {
    return sections.map(s => s.name).join(', ');
  }

  isoStrToDdMM(localtimedate_str: string) {
    return Util.isoStrToDdMM(localtimedate_str);
  }
}

declarePropertyDependencies(Reports, 'existsPrevious', ['currentRehearsal']);
declarePropertyDependencies(Reports, 'existsNext', ['currentRehearsal']);
declarePropertyDependencies(Reports, 'attendanceSelectedCount', ['attendanceSelected']);
declarePropertyDependencies(Reports, 'attendanceUrl', ['currentRehearsal']);
